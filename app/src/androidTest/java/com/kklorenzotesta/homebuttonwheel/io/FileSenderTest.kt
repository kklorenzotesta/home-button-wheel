package com.kklorenzotesta.homebuttonwheel.io

import android.content.Intent
import android.net.Uri
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kklorenzotesta.homebuttonwheel.targetContext
import com.kklorenzotesta.homebuttonwheel.testutil.matchers.ChooserIntentInnerMatcher
import com.kklorenzotesta.homebuttonwheel.testutil.matchers.matches
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import java.io.File
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.equalTo
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.runner.RunWith

/**
 * If run in Robolectric is impossible to check the received content,
 * so this class should stay in androidTest
 */
@RunWith(AndroidJUnit4::class)
class FileSenderTest {
    @get:Rule
    val tempFolderRule = TemporaryFolder()

    @get:Rule
    val intentsTestRule = IntentsTestRule(SettingsActivity::class.java)

    private val sender = FileSender(targetContext())

    @Test
    fun stringIsSentWithTheCorrectContentAndName() {
        val content = "random content"
        val name = "filename"
        sender.send(content, "filename")
        checkSentContent(content, name)
    }

    @Test
    fun stringIsSentWithTheCorrectContentAndNameAndTitle() {
        val content = "random content"
        val name = "filename"
        val title = "My title"
        sender.send(content, "filename", title)
        checkSentContent(content, name, title)
    }

    @Test
    fun fileIsSentWithTheCorrectContent() {
        val fileToSend = tempFolderRule.newFile()
        val content = "random content"
        fileToSend.writeText(content)
        sender.send(fileToSend)
        checkSentContent(content, fileToSend.name)
    }

    @Test
    fun fileIsSentWithGivenNameAndTitle() {
        val fileToSend = tempFolderRule.newFile()
        val content = "random content"
        val newName = "newName.txt"
        val title = "My title"
        fileToSend.writeText(content)
        sender.send(fileToSend, newName, title)
        checkSentContent(content, newName, title)
    }

    private fun checkSentContent(expected: String, name: String, title: String? = null) {
        intended(
            allOf(
                hasAction(Intent.ACTION_CHOOSER),
                hasFlag(Intent.FLAG_ACTIVITY_NEW_TASK),
                if (title != null)
                    hasExtra(Intent.EXTRA_TITLE, title)
                else matches("") { true },
                ChooserIntentInnerMatcher(
                    allOf(
                        hasAction(Intent.ACTION_SEND),
                        hasType("text/plain"),
                        hasExtra(
                            equalTo(Intent.EXTRA_STREAM),
                            matches("content is \"$expected\" with name $name") { uri: Uri ->
                                uri.toString().endsWith(File.separator + name) &&
                                        targetContext()
                                            .contentResolver
                                            .openInputStream(uri)!!
                                            .bufferedReader()
                                            .use {
                                                it.readText()
                                            } == expected
                            }
                        )
                    )
                )
            )
        )
    }
}
