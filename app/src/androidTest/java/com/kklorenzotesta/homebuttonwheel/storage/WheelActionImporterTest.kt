package com.kklorenzotesta.homebuttonwheel.storage

import android.net.Uri
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kklorenzotesta.homebuttonwheel.serialization.Serializer
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import io.mockk.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerializationException
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.runner.RunWith
import org.koin.test.get

@RunWith(AndroidJUnit4::class)
class WheelActionImporterTest : HBWTest() {
    @get:Rule
    val tempFolderRule = TemporaryFolder()

    @Test
    fun importFailsOnNotExistingUri() = runBlocking {
        val uri =
            Uri.withAppendedPath(Uri.fromFile(tempFolderRule.newFolder()), "NotExistingFile.txt")
        assertEquals(
            ImportResult.ImportError,
            WheelActionImporter(get(), mockk(), context).import(uri)
        )
    }

    @Test
    fun importFailsOnSerializerException() = runBlocking {
        val uri =
            Uri.fromFile(tempFolderRule.newFile())
        val serializer: Serializer = mockk()
        every { serializer.parseActions(any()) } throws SerializationException("")
        assertEquals(
            ImportResult.ImportError,
            WheelActionImporter(serializer, mockk(), context).import(uri)
        )
    }

    @Test
    fun importOnValidDeserializationCallsSetActionsAndReturnsSuccess() = runBlocking {
        val uri =
            Uri.fromFile(tempFolderRule.newFile())
        val serializer: Serializer = mockk()
        every { serializer.parseActions(any()) } returns emptyList()
        val storage: WheelActionStorage = mockk()
        coEvery { storage.setActions(any()) } just Runs
        assertEquals(
            ImportResult.Success,
            WheelActionImporter(serializer, storage, context).import(uri)
        )
        coVerify { storage.setActions(emptyList()) }
    }
}
