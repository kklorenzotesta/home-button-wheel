package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.content.Intent
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.intent.Intents.*
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditSettingsFragmentImportTest : HBWTest() {
    @get:Rule
    val intentsTestRule = IntentsTestRule(SettingsActivity::class.java)

    @Test
    fun importSettingsStartCorrectActivity() {
        openActionBarOverflowOrOptionsMenu(context)
        onView(withText(context.getString(R.string.import_settings_menu_item_title)))
            .perform(click())
        intended(
            hasAction(Intent.ACTION_GET_CONTENT)
        )
    }
}
