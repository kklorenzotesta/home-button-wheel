package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import org.junit.Test

class EditSettingsFragmentMoveTestNoSkip : EditSettingsFragmentMoveTest() {
    @Test
    override fun canReorderWithHandle() {
        super.canReorderWithHandle()
    }

    @Test
    override fun canReorderWithLongPress() {
        super.canReorderWithLongPress()
    }
}
