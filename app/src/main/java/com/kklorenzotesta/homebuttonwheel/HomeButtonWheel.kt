package com.kklorenzotesta.homebuttonwheel

import android.app.Application
import com.kklorenzotesta.homebuttonwheel.modules.mainModule
import com.kklorenzotesta.homebuttonwheel.modules.serializationModule
import com.kklorenzotesta.homebuttonwheel.modules.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

/** Custom Application implementation responsible for the Koin initialization */
class HomeButtonWheel : Application() {
    companion object {
        /** Returns the list of all the modules that must be loaded by Koin */
        fun modules(): List<Module> = listOf(
            mainModule(),
            viewModelsModule(),
            serializationModule()
        )
    }

    override fun onCreate() {
        super.onCreate()
        if (GlobalContext.getOrNull() == null) {
            startKoin {
                androidLogger()
                androidContext(this@HomeButtonWheel)
                modules(modules())
            }
        }
    }
}
