package com.kklorenzotesta.homebuttonwheel.actions

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import kotlinx.serialization.Serializable

/**
 * Represents a quick action that can be executed from the [com.kklorenzotesta.homebuttonwheel.WheelActivity].
 */
sealed class WheelAction {
    /** Executes this action */
    abstract fun run(context: Context)
}

/**
 * A [WheelAction] that launches an activity.
 *
 * @param packageName the package of the activity
 * @param className the name of the class of the activity
 */
@Serializable
data class LaunchActivity(val packageName: String, val className: String) : WheelAction() {
    override fun run(context: Context) {
        Intent(Intent.ACTION_MAIN).apply {
            addCategory(Intent.CATEGORY_LAUNCHER)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            component = ComponentName(packageName, className)
            context.startActivity(this)
        }
    }
}
