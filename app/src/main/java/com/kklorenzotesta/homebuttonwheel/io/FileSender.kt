package com.kklorenzotesta.homebuttonwheel.io

import android.content.Context
import android.content.Intent
import androidx.core.content.FileProvider
import com.kklorenzotesta.homebuttonwheel.R
import java.io.File

/** Provides methods to send files to other apps */
class FileSender(private val context: Context) {
    /** File representing the directory on which the files will be staged before sending */
    private val exportsDirectory: File by lazy {
        File(context.cacheDir, context.getString(R.string.export_folder_name)).apply {
            mkdirs()
        }
    }

    /** Creates a file with the given name in a directory dedicated to the export */
    private fun generateFile(name: String): File =
        File(exportsDirectory, name)

    /**
     * Send a file with the given content.
     *
     * @param content the content of the file that will be sent
     * @param name the name of the file in which the content will be sent
     * @param exportTargetChooserTitle optionally the title for the target app chooser
     *                                 receiving the file
     */
    fun send(content: String, name: String, exportTargetChooserTitle: String? = null) {
        send(
            generateFile(name).also { outputFile ->
                outputFile.writeText(content)
            },
            null,
            exportTargetChooserTitle
        )
    }

    /**
     * Send the given file as a plain text file. Destination app
     * will be opened in a new task.
     *
     * @param file the file to be sent
     * @param name optionally a new name under which the file will be received
     * @param exportTargetChooserTitle optionally the title for the target app chooser
     *                                 receiving the file
     * @throws NoSuchFileException if the given file doesn't exists
     */
    fun send(file: File, name: String? = null, exportTargetChooserTitle: String? = null) {
        val fileToSend = generateFile(name ?: file.name)
        if (fileToSend != file) {
            file.copyTo(fileToSend, overwrite = true)
        }
        val uri = FileProvider.getUriForFile(
            context,
            context.getString(R.string.file_share_authority),
            fileToSend
        )
        val intent = Intent.createChooser(Intent(Intent.ACTION_SEND).apply {
            putExtra(
                Intent.EXTRA_STREAM,
                uri
            )
            type = "text/plain"
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }, exportTargetChooserTitle).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        context.startActivity(intent)
    }
}
