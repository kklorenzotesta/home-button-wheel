package com.kklorenzotesta.homebuttonwheel.modules

import androidx.preference.PreferenceManager
import com.kklorenzotesta.homebuttonwheel.io.FileSender
import com.kklorenzotesta.homebuttonwheel.settings.activitypicker.ActivityInfoLoader
import com.kklorenzotesta.homebuttonwheel.storage.*
import com.kklorenzotesta.homebuttonwheel.ui.actions.FullWheelActionInformationProvider
import com.kklorenzotesta.homebuttonwheel.ui.actions.LaunchActivityInformationProvider
import com.kklorenzotesta.homebuttonwheel.ui.actions.WheelActionInformationProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.*

/** Returns a Koin module declaring the main components */
fun mainModule(): Module = module {
    single { ActivityInfoLoader(androidContext()) }
    single {
        @Suppress("EXPERIMENTAL_API_USAGE")
        PreferenceManager.getDefaultSharedPreferences(androidContext()).toStringStorage()
    } bind StringStorage::class
    single { WheelActionStorage(get(), get()) }
    single { LaunchActivityInformationProvider(androidContext()) }
    single { FullWheelActionInformationProvider(get()) } bind WheelActionInformationProvider::class
    single { FileSender(androidContext()) }
    single { WheelActionExporter(get(), get(), androidContext()) }
    single { WheelActionImporter(get(), get(), androidContext()) }
}
