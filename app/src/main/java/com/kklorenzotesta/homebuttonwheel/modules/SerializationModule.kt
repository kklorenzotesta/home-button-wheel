package com.kklorenzotesta.homebuttonwheel.modules

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.serialization.Serializer
import kotlinx.serialization.KSerializer
import kotlinx.serialization.PolymorphicSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import kotlinx.serialization.modules.SerializersModule
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val ACTIONS_SERIALIZER_NAME = "actions-serializer"

/**
 * Returns a Koin module declaring all the components needed to serialize or
 * deserialize [WheelAction]s in a polymorphic fashion
 */
fun serializationModule(): Module = module {
    single {
        SerializersModule {
            polymorphic<WheelAction> {
                addSubclass(LaunchActivity.serializer())
            }
        }
    }
    single {
        Json(
            context = get(),
            configuration = JsonConfiguration.Stable.copy(strictMode = false)
        )
    }
    single(named(ACTIONS_SERIALIZER_NAME)) {
        @Suppress("UNCHECKED_CAST")
        PolymorphicSerializer(WheelAction::class).list as KSerializer<List<WheelAction>>
    }
    single {
        Serializer(get(), get(named(ACTIONS_SERIALIZER_NAME)))
    }
}
