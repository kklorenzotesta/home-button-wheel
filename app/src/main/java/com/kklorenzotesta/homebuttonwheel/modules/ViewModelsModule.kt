package com.kklorenzotesta.homebuttonwheel.modules

import com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker.MainActivityPickerViewModel
import com.kklorenzotesta.homebuttonwheel.ui.settings.edit.EditSettingsViewModel
import org.koin.androidx.viewmodel.dsl.*
import org.koin.core.module.Module
import org.koin.dsl.*

/** Returns a Koin module declaring all the [androidx.lifecycle.ViewModel]s */
fun viewModelsModule(): Module = module {
    viewModel { MainActivityPickerViewModel(get(), get(), get()) }
    viewModel { EditSettingsViewModel(get(), get(), get(), get()) }
}
