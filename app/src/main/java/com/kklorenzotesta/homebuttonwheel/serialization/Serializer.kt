package com.kklorenzotesta.homebuttonwheel.serialization

import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

/** Provides methods to serialize/deserialize some supported types */
class Serializer(
    private val json: Json,
    private val actionsSerializer: KSerializer<List<WheelAction>>
) {
    /** @see [Json.stringify] */
    fun stringify(actions: List<WheelAction>): String =
        json.stringify(actionsSerializer, actions)

    /** @see [Json.parse] */
    fun parseActions(string: String): List<WheelAction> =
        json.parse(actionsSerializer, string)
}
