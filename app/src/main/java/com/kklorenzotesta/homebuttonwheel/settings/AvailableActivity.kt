package com.kklorenzotesta.homebuttonwheel.settings

import android.content.ComponentName
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction

/**
 * Represents an activity that can be added as an action
 *
 * @param packageName the name of the package of the activity
 * @param fullName the name of the class of the activity
 */
data class AvailableActivity(val packageName: String, val fullName: String) {
    /** The [ComponentName] corresponding to the activity */
    val componentName: ComponentName by lazy { ComponentName(packageName, fullName) }

    /** Returns the corresponding [WheelAction] */
    fun toWheelAction(): WheelAction =
        LaunchActivity(packageName, fullName)
}
