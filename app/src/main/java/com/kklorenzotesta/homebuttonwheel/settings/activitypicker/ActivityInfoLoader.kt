package com.kklorenzotesta.homebuttonwheel.settings.activitypicker

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import java.lang.ref.SoftReference
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

/**
 * Provides information about the activities that can be launched
 *
 * @param context the [Context] from which the information will be loaded
 */
class ActivityInfoLoader(context: Context) {
    /** A [PackageManager] instance to find global package information */
    private val packageManager: PackageManager by lazy { context.packageManager }

    /** Intent that lists all them main activities */
    private val activityListIntent: Intent by lazy {
        Intent(Intent.ACTION_MAIN).apply {
            addCategory(Intent.CATEGORY_LAUNCHER)
        }
    }

    /** Cache for the last extracted list of activities */
    private var cache = SoftReference<List<AvailableActivity>>(null)

    /**
     * Returns a Flow of a list with all the current main [AvailableActivity]s.
     * There is no guarantee on if or when the flow will emit new value, but at least
     * one is always emitted.
     */
    fun getAvailableActivities(): Flow<List<AvailableActivity>> =
        flow {
            cache.get()?.let { emit(it) }
            emit(
                getCurrentActivityList().map {
                    AvailableActivity(it.packageName, it.name)
                }.also {
                    cache = SoftReference(it)
                }
            )
        }

    private suspend fun getCurrentActivityList(): List<ActivityInfo> = withContext(IO) {
        packageManager.queryIntentActivities(activityListIntent, 0).map {
            it.activityInfo
        }
    }
}
