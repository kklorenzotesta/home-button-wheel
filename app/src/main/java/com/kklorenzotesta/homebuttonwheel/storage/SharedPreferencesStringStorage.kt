package com.kklorenzotesta.homebuttonwheel.storage

import android.annotation.SuppressLint
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*

/**
 * Implementation of [StringStorage] based on [SharedPreferences].
 *
 * @param preferences the [SharedPreferences] on which the actual persistence will happen
 */
@ExperimentalCoroutinesApi
class SharedPreferencesStringStorage(private val preferences: SharedPreferences) : StringStorage {
    override operator fun get(key: String): Flow<String?> =
        callbackFlow {
            launch(IO) {
                preferences.getString(key, null).let(::offer)
            }
            val listener = SharedPreferences.OnSharedPreferenceChangeListener { pref, changedKey ->
                if (changedKey == key) {
                    launch(IO) {
                        pref.getString(key, null).let(::offer)
                    }
                }
            }
            preferences.registerOnSharedPreferenceChangeListener(listener)
            awaitClose { preferences.unregisterOnSharedPreferenceChangeListener(listener) }
        }

    @SuppressLint("ApplySharedPref")
    override suspend fun set(key: String, value: String?) {
        withContext(IO) {
            preferences.edit(true) {
                putString(key, value)
            }
        }
    }
}

/**
 * Creates a [StringStorage] based on this [SharedPreferences]
 */
@ExperimentalCoroutinesApi
fun SharedPreferences.toStringStorage(): StringStorage = SharedPreferencesStringStorage(this)
