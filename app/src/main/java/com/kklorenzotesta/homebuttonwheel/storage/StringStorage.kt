package com.kklorenzotesta.homebuttonwheel.storage

import kotlinx.coroutines.flow.Flow

/**
 * Suspending persistent storage of string data
 */
interface StringStorage {
    /**
     * Returns a [Flow] that will stream the current value and all changes to the
     * string stored with the given key.
     *
     * @param key the key for the desired string
     * @return a Flow streaming the current values, or streaming null when is missing
     */
    operator fun get(key: String): Flow<String?>

    /**
     * Updates the stored string for the given key
     *
     * @param key the key under which the string will be stored
     * @param value the new value, or null to remove any current value
     */
    suspend fun set(key: String, value: String?)
}
