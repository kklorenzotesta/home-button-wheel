package com.kklorenzotesta.homebuttonwheel.storage

import android.content.Context
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.io.FileSender
import com.kklorenzotesta.homebuttonwheel.serialization.Serializer
import java.text.SimpleDateFormat
import java.util.*

/**
 * Represents the result of an export attempt
 *
 * @see [WheelActionExporter.export]
 */
sealed class ExportResult {
    /** Represent the case that there was nothing to be exported in the input */
    object NothingToExport : ExportResult()

    /** Represents a successful export */
    object Success : ExportResult()
}

/**
 * Provides the export functionality for the [WheelAction]s
 *
 * @param serializer the Serializer that will be used to write the actions
 * @param fileSender the [FileSender] with which will be sent the serialized form of the actions
 * @param context the context from which will be extracted the configuration about file names, ecc...
 */
class WheelActionExporter(
    private val serializer: Serializer,
    private val fileSender: FileSender,
    private val context: Context
) {
    /**
     * Sends outside of this app a File containing a representation of the given actions
     *
     * @param actions the actions that will be exported
     * @return the [ExportResult]: [ExportResult.NothingToExport] if the list was empty,
     *         [ExportResult.Success] otherwise
     */
    fun export(actions: List<WheelAction>): ExportResult =
        if (actions.isEmpty()) {
            ExportResult.NothingToExport
        } else {
            val stringRepresentation = serializer.stringify(actions)
            fileSender.send(
                stringRepresentation,
                generateFileName(),
                context.getString(R.string.send_backup_chooser_title)
            )
            ExportResult.Success
        }

    /** Returns the name that will be used for the file sent */
    private fun generateFileName(): String =
        context.getString(R.string.app_name) +
                SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date()) +
                "." + context.getString(R.string.backup_files_extension)
}
