package com.kklorenzotesta.homebuttonwheel.storage

import android.content.Context
import android.net.Uri
import com.kklorenzotesta.homebuttonwheel.serialization.Serializer
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader
import kotlinx.serialization.SerializationException

/**
 * Represents the result of an import attempt
 *
 * @see [WheelActionImporter.import]
 */
sealed class ImportResult {
    /** Represent the case of an error during the import */
    object ImportError : ImportResult()

    /** Represents a successful import */
    object Success : ImportResult()
}

/**
 * Provides the import functionality for the [com.kklorenzotesta.homebuttonwheel.actions.WheelAction]s
 *
 * @param serializer the Serializer that will be used to read the actions
 * @param storage the [WheelActionStorage] on which will be stored the imported actions
 * @param context the context from which will resolved the uri
 */
class WheelActionImporter(
    private val serializer: Serializer,
    private val storage: WheelActionStorage,
    private val context: Context
) {
    /**
     * Imports the actions that has beed exported before at the given uri.
     *
     * @param uri the uri from which the actions will be read
     * @return the [ImportResult]: [ImportResult.ImportError] if the content at the uri was not valid,
     *         [ImportResult.Success] otherwise
     */
    suspend fun import(uri: Uri): ImportResult {
        return try {
            val inputStream = context.contentResolver.openInputStream(uri)
            if (inputStream != null) {
                val text = BufferedReader(InputStreamReader(inputStream)).readText()
                storage.setActions(serializer.parseActions(text))
                ImportResult.Success
            } else {
                ImportResult.ImportError
            }
        } catch (_: SerializationException) {
            ImportResult.ImportError
        } catch (_: FileNotFoundException) {
            ImportResult.ImportError
        }
    }
}
