package com.kklorenzotesta.homebuttonwheel.storage

import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.serialization.Serializer
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

/** Manages the persistence of the [WheelAction]s */
class WheelActionStorage(private val serializer: Serializer, private val store: StringStorage) {
    companion object {
        /** Key used to store the actions in the [StringStorage] */
        private const val storageKey: String = "WHEEL_ACTIONS"
    }

    /** Returns a [Flow] that will stream the current actions after every update */
    fun getActions(): Flow<List<WheelAction>> =
        store[storageKey]
            .map { s ->
                s?.let(serializer::parseActions)
            }.map {
                it ?: emptyList()
            }

    /** Persists the given actions */
    suspend fun setActions(actions: List<WheelAction>) {
        persistActions(actions)
    }

    /**
     * Persists the given [WheelAction].
     *
     * @param action a new action to be persisted
     */
    suspend fun addWheelAction(action: WheelAction) {
        persistActions(getActions().first() + action)
    }

    /**
     * Removes the given [WheelAction]
     *
     * @param action the action to be remove from the persisted actions
     */
    suspend fun removeWheelAction(action: WheelAction) {
        persistActions(getActions().first() - action)
    }

    /** Persists the given actions */
    private suspend fun persistActions(actions: List<WheelAction>) {
        store.set(
            storageKey,
            if (actions.isEmpty()) {
                null
            } else {
                serializer.stringify(actions)
            }
        )
    }
}
