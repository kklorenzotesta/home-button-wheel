package com.kklorenzotesta.homebuttonwheel.ui.actions

import android.graphics.drawable.Drawable
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction

/**
 * [WheelActionInformationProvider] implementation that combines specific ones to support any
 * type of action.
 */
class FullWheelActionInformationProvider(
    private val launchActivity: LaunchActivityInformationProvider
) : WheelActionInformationProvider<WheelAction> {
    override suspend fun getIcon(action: WheelAction): Drawable = when (action) {
        is LaunchActivity -> launchActivity.getIcon(action)
    }

    override suspend fun getLabel(action: WheelAction): String = when (action) {
        is LaunchActivity -> launchActivity.getLabel(action)
    }
}
