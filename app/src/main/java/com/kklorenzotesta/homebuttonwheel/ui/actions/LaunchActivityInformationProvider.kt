package com.kklorenzotesta.homebuttonwheel.ui.actions

import android.content.ComponentName
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

/** [WheelActionInformationProvider] implementation for [LaunchActivity] */
class LaunchActivityInformationProvider(
    context: Context
) : WheelActionInformationProvider<LaunchActivity> {
    /** A [PackageManager] instance to find global package information */
    private val packageManager: PackageManager by lazy { context.packageManager }

    override suspend fun getIcon(action: LaunchActivity): Drawable = withContext(IO) {
        packageManager.getActivityIcon(action.componentName())
    }

    override suspend fun getLabel(action: LaunchActivity): String = withContext(IO) {
        action.activityInfo().loadLabel(packageManager).toString()
    }

    private fun LaunchActivity.activityInfo(): ActivityInfo =
        packageManager.getActivityInfo(componentName(), 0)

    private fun LaunchActivity.componentName(): ComponentName =
        ComponentName(packageName, className)
}
