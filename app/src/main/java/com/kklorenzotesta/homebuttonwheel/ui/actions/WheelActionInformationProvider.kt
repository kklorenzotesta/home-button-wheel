package com.kklorenzotesta.homebuttonwheel.ui.actions

import android.graphics.drawable.Drawable
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction

/**
 * Provides information needed to display [WheelAction]s
 *
 * @param T the type of [WheelAction] supported
 */
interface WheelActionInformationProvider<T : WheelAction> {
    /** Returns the icon that should be displayed for the given action */
    suspend fun getIcon(action: T): Drawable

    /** Returns the label that should be displayed for the given action */
    suspend fun getLabel(action: T): String
}
