package com.kklorenzotesta.homebuttonwheel.ui.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.kklorenzotesta.homebuttonwheel.R
import kotlinx.android.synthetic.main.settings_activity_layout.*

/**
 * The settings activity
 */
class SettingsActivity : AppCompatActivity(R.layout.settings_activity_layout) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navController = findNavController(R.id.settings_nav_host_fragment)
        toolbar.apply {
            setSupportActionBar(this)
            setupWithNavController(navController)
        }
    }
}
