package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import android.graphics.drawable.Drawable
import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import com.kklorenzotesta.homebuttonwheel.util.Async
import com.kklorenzotesta.homebuttonwheel.util.AsyncJob

/**
 * Wrapper for [AvailableActivity] adding all the information needed to be displayed to the user
 *
 * @param activity the wrapper [AvailableActivity]
 * @param label the label for the [AvailableActivity]
 * @param icon the [Async] loading the corresponding drawable icon
 * @param onAddActivity the [AsyncJob] that must be run when the user request to add the action for
 *                      this activity
 */
class DisplayableAvailableActivity(
    val activity: AvailableActivity,
    val label: String,
    val icon: Async<Drawable>,
    val onAddActivity: AsyncJob
) {
    override fun equals(other: Any?): Boolean =
        this === other || (other is DisplayableAvailableActivity &&
            this.activity == other.activity)

    override fun hashCode(): Int =
        activity.hashCode()

    override fun toString(): String = javaClass.name + "($label)"
}
