package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import com.kklorenzotesta.homebuttonwheel.ui.util.BindableViewHolderListAdapter

/**
 * Adapter for displaying a list of [AvailableActivity]
 */
class MainActivitiesAdapter :
    BindableViewHolderListAdapter<DisplayableAvailableActivity, MainActivityViewHolder>(
        MainActivityViewHolder
    )
