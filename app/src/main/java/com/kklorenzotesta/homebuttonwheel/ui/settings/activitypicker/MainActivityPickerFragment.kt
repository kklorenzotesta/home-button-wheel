package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.ui.util.Computation
import kotlinx.android.synthetic.main.activity_picker_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/** The fragment showing all the possible main activities */
class MainActivityPickerFragment : Fragment(R.layout.activity_picker_layout) {
    /** The ViewModel holding the activities */
    private val activityPickerViewModel: MainActivityPickerViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val activitiesAdapter = MainActivitiesAdapter()
        main_activities_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = activitiesAdapter
        }
        activityPickerViewModel.mainActivities.observe(viewLifecycleOwner) {
            when (it) {
                Computation.Loading -> {
                    main_activities_list.visibility = View.GONE
                    main_activities_loading.visibility = View.VISIBLE
                }
                is Computation.Ready -> {
                    activitiesAdapter.submitList(it.value)
                    main_activities_list.visibility = View.VISIBLE
                    main_activities_loading.visibility = View.GONE
                }
            }
        }
    }
}
