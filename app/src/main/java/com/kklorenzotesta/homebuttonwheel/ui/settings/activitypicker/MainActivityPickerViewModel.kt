package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.settings.activitypicker.ActivityInfoLoader
import com.kklorenzotesta.homebuttonwheel.storage.WheelActionStorage
import com.kklorenzotesta.homebuttonwheel.ui.actions.WheelActionInformationProvider
import com.kklorenzotesta.homebuttonwheel.ui.util.Computation
import com.kklorenzotesta.homebuttonwheel.ui.util.viewModelContext
import com.kklorenzotesta.homebuttonwheel.util.async
import com.kklorenzotesta.homebuttonwheel.util.cached
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect

/**
 * ViewModel provided to the views the list of the main activities available
 *
 * @param activityInfoLoader the [ActivityInfoLoader] from which will get the list of activities
 * @param informationProvider the [WheelActionInformationProvider] that will be used to load
 *                            labels and icons
 * @param storage the [WheelActionStorage] in which will be add the activity action if needed
 */
class MainActivityPickerViewModel(
    private val activityInfoLoader: ActivityInfoLoader,
    private val informationProvider: WheelActionInformationProvider<WheelAction>,
    private val storage: WheelActionStorage
) : ViewModel() {
    /** LiveData loading the list of main [DisplayableAvailableActivity]s */
    val mainActivities: LiveData<Computation<List<DisplayableAvailableActivity>>> =
        liveData(viewModelContext + Default) {
            emit(Computation.Loading)
            activityInfoLoader.getAvailableActivities().collect { list ->
                emit(Computation.Ready(list.map {
                    Pair(it, coroutineScope {
                        this.async {
                            informationProvider.getLabel(
                                it.toWheelAction()
                            )
                        }
                    })
                }.map {
                    DisplayableAvailableActivity(
                        it.first,
                        it.second.await(),
                        async {
                            informationProvider.getIcon(it.first.toWheelAction())
                        }.cached(),
                        async {
                            storage.addWheelAction(it.first.toWheelAction())
                        }
                    )
                }.sortedBy { it.label }))
            }
        }
}
