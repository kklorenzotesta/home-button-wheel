package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import com.kklorenzotesta.homebuttonwheel.ui.util.LifecycledViewHolder
import com.kklorenzotesta.homebuttonwheel.ui.util.ViewHolderBuilder
import com.kklorenzotesta.homebuttonwheel.util.coStartAsync
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

/**
 * ViewHolder for the main [AvailableActivity]s
 *
 * @param view the holded view
 */
class MainActivityViewHolder private constructor(
    view: View
) :
    LifecycledViewHolder<DisplayableAvailableActivity>(view) {
    companion object : ViewHolderBuilder<MainActivityViewHolder>(
        R.layout.activities_list_main_activity_layout,
        ::MainActivityViewHolder
    )

    /** TextView displaying the label of the activity */
    private val labelView: TextView by lazy {
        view.findViewById<TextView>(R.id.main_activity_name)
    }

    /** ImageView displaying the icon of the activity */
    private val iconView: ImageView by lazy {
        view.findViewById<ImageView>(R.id.activity_icon)
    }

    override fun onBindItem(item: DisplayableAvailableActivity) {
        super.onBindItem(item)
        labelView.text = item.label
        lifecycleScope.launch(Main) {
            iconView.setImageDrawable(null)
            iconView.setImageDrawable(coStartAsync(item.icon).await())
        }
        itemView.setOnClickListener {
            item.onAddActivity.apply {
                runningScope.launch {
                    startAsync().join()
                    it.findNavController().navigate(R.id.action_backToEditSettings)
                }
            }
        }
    }
}
