package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.graphics.drawable.Drawable
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.util.Async
import com.kklorenzotesta.homebuttonwheel.util.AsyncJob

/**
 * Wrapper for [WheelAction] adding all the information needed to be displayed to the user
 *
 * @param action the wrapped [WheelAction]
 * @param label the [Async] loading the corresponding label
 * @param icon the [Async] loading the corresponding drawable icon
 * @param deleteAction the [AsyncJob] that must be run when the user request to delete this action
 */
class DisplayableWheelAction(
    val action: WheelAction,
    val label: Async<String>,
    val icon: Async<Drawable>,
    val deleteAction: AsyncJob
) {
    override fun equals(other: Any?): Boolean =
        this === other || (other is DisplayableWheelAction &&
            this.action == other.action)

    override fun hashCode(): Int =
        action.hashCode()

    override fun toString(): String = javaClass.name + "($action)"
}
