package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.storage.ExportResult
import com.kklorenzotesta.homebuttonwheel.storage.ImportResult
import com.kklorenzotesta.homebuttonwheel.ui.util.observeOnce
import kotlinx.android.synthetic.main.edit_settings_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/** The fragment showing all the current settings and the buttons for edit them */
class EditSettingsFragment : Fragment(R.layout.edit_settings_layout) {
    companion object {
        /** The request code for the intent launched to import settings from a file */
        const val IMPORT_SETTINGS_FILE_REQUEST_CODE = 300
    }

    /** The ViewModel proving the actions that will be displayed */
    private val actionsViewModel: EditSettingsViewModel by viewModel()

    init {
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val actionsAdapter = WheelActionAdapter(actionsViewModel)
        actions_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = actionsAdapter
        }
        actionsViewModel.actions.observe(viewLifecycleOwner) {
            actionsAdapter.submitList(it.toMutableList())
        }
        add_action_button.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_addActivityAction)
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.settings_activity_main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.export_settings_menu_item -> {
                actionsViewModel.exportActions().observeOnce(viewLifecycleOwner) {
                    when (it) {
                        ExportResult.NothingToExport ->
                            Toast.makeText(
                                context,
                                getString(R.string.nothing_to_export_error_message),
                                Toast.LENGTH_SHORT
                            ).show()
                        ExportResult.Success -> Unit
                    }
                }
                true
            }
            R.id.import_settings_menu_item -> {
                startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
                    type = "*/*"
                }, IMPORT_SETTINGS_FILE_REQUEST_CODE)
                true
            }
            else -> false
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IMPORT_SETTINGS_FILE_REQUEST_CODE ->
                data?.data?.let(actionsViewModel::importActions)?.observeOnce(
                    viewLifecycleOwner
                ) {
                    when (it) {
                        ImportResult.Success -> Unit
                        ImportResult.ImportError ->
                            Toast.makeText(
                                context,
                                "Can't import settings from the given file",
                                Toast.LENGTH_LONG
                            ).show()
                    }
                }
        }
    }
}
