package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.storage.*
import com.kklorenzotesta.homebuttonwheel.ui.actions.WheelActionInformationProvider
import com.kklorenzotesta.homebuttonwheel.ui.util.toLiveData
import com.kklorenzotesta.homebuttonwheel.ui.util.viewModelContext
import com.kklorenzotesta.homebuttonwheel.util.async
import com.kklorenzotesta.homebuttonwheel.util.cached
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

/**
 * ViewModel that provides to the views the list of actions currently set
 *
 * @param storage the [WheelActionStorage] from which the action will be retrieved
 * @param exporter the [WheelActionExporter] that will be used to export the settings
 * @param importer the [WheelActionImporter] that will be used to import the settings
 * @param informationProvider the [WheelActionInformationProvider] from which label and icon
 *                            will be loaded
 */
class EditSettingsViewModel(
    private val storage: WheelActionStorage,
    private val exporter: WheelActionExporter,
    private val importer: WheelActionImporter,
    informationProvider: WheelActionInformationProvider<WheelAction>
) : ViewModel() {
    /** A LiveData with the current actions */
    @Suppress("EXPERIMENTAL_API_USAGE")
    val actions: LiveData<List<DisplayableWheelAction>> =
        storage.getActions().map { list ->
            list.map { action ->
                DisplayableWheelAction(
                    action,
                    async {
                        informationProvider.getLabel(action)
                    }.cached(),
                    async {
                        informationProvider.getIcon(action)
                    }.cached(),
                    async {
                        storage.removeWheelAction(action)
                    }
                )
            }
        }.toLiveData(viewModelContext)

    /** Exports the current settings with a send Intent */
    fun exportActions(): LiveData<ExportResult> =
        liveData(viewModelContext) {
            emit(exporter.export(storage.getActions().first()))
        }

    /** Imports the settings from the file at the given Uri */
    fun importActions(uri: Uri): LiveData<ImportResult> =
        liveData(viewModelContext) {
            emit(importer.import(uri))
        }

    /** Persists the given [DisplayableWheelAction] */
    fun setActions(actions: List<DisplayableWheelAction>) {
        viewModelScope.launch {
            storage.setActions(actions.map { it.action })
        }
    }
}
