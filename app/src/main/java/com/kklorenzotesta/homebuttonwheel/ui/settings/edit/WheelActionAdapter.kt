package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.ui.util.BindableViewHolderListAdapter

/**
 * Adapter for [WheelAction]s in the edit settings fragment
 *
 * @param viewModel the viewModel on which will be persisted the changes
 */
class WheelActionAdapter(
    private val viewModel: EditSettingsViewModel
) : BindableViewHolderListAdapter<DisplayableWheelAction, WheelActionViewHolder>(
    WheelActionViewHolder
) {
    /**
     * Implementation of [ItemTouchHelper.Callback] for handle swipe to delete and
     * hold to reorder.
     *
     * **NB: can't use an inner class or anonymous class otherwise the lint error:
     * "requires synthetic accessor" is emitted**
     *
     * @param adapter the [WheelActionAdapter] for this callback
     * @param items the list with the current items, will be updated on move
     * @param persistChanges callback to persist the changes at the end of movements
     */
    private class TouchCallback(
        private val adapter: WheelActionAdapter,
        private val items: () -> MutableList<DisplayableWheelAction>,
        private val persistChanges: () -> Unit
    ) : ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN,
        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
    ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = true.also {
            val item = adapter.getItem(viewHolder.adapterPosition)
            items().removeAt(viewHolder.adapterPosition)
            items().add(target.adapterPosition, item)
            adapter.notifyItemMoved(viewHolder.adapterPosition, target.adapterPosition)
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            adapter.getItem(viewHolder.adapterPosition).deleteAction.startAsync()
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder)
            persistChanges()
        }
    }

    /** The always updated list of the currently displayed items */
    private var items: MutableList<DisplayableWheelAction> = mutableListOf()

    /** The [ItemTouchHelper] used to implement swipe to delete behavior */
    private val touchHelper = ItemTouchHelper(TouchCallback(this, { items }, ::persistChanges))

    /** Persists the changes to items in the [viewModel] when a touch action ends */
    private fun persistChanges() {
        viewModel.setActions(items)
    }

    override fun submitList(list: MutableList<DisplayableWheelAction>?) {
        super.submitList(list)
        items = list ?: mutableListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WheelActionViewHolder =
        super.onCreateViewHolder(parent, viewType).apply {
            setupHandler(touchHelper)
        }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        touchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        touchHelper.attachToRecyclerView(null)
    }
}
