package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.ui.util.LifecycledViewHolder
import com.kklorenzotesta.homebuttonwheel.ui.util.ViewHolderBuilder
import com.kklorenzotesta.homebuttonwheel.util.coStartAsync
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

/**
 * [LifecycledViewHolder] for [WheelAction]s in the settings activity
 *
 * @param view the holded view
 */
class WheelActionViewHolder private constructor(
    view: View
) : LifecycledViewHolder<DisplayableWheelAction>(view) {
    /** [ViewHolderBuilder] for [WheelActionViewHolder]s */
    companion object : ViewHolderBuilder<WheelActionViewHolder>(
        R.layout.actions_list_action_layout,
        ::WheelActionViewHolder
    )

    /** TextView displaying the label of the activity */
    private val labelView: TextView by lazy {
        view.findViewById<TextView>(R.id.action_label)
    }

    /** ImageView displaying the icon of the activity */
    private val iconView: ImageView by lazy {
        view.findViewById<ImageView>(R.id.action_icon)
    }

    /** ImageView displaying the handle for reorder the actions */
    private val reorderIcon: ImageView by lazy {
        view.findViewById<ImageView>(R.id.list_item_handle)
    }

    override fun onBindItem(item: DisplayableWheelAction) {
        super.onBindItem(item)
        lifecycleScope.launch(Main) {
            labelView.text = ""
            labelView.text = coStartAsync(item.label).await()
        }
        lifecycleScope.launch(Main) {
            iconView.setImageDrawable(null)
            iconView.setImageDrawable(coStartAsync(item.icon).await())
        }
    }

    /** The adapter must call this method to setup correctly the reorder handle  */
    @SuppressLint("ClickableViewAccessibility")
    fun setupHandler(helper: ItemTouchHelper) {
        reorderIcon.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                helper.startDrag(this)
                true
            } else {
                false
            }
        }
    }
}
