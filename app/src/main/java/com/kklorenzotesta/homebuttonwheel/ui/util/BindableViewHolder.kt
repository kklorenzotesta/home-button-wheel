package com.kklorenzotesta.homebuttonwheel.ui.util

import android.view.View
import androidx.annotation.MainThread
import androidx.recyclerview.widget.RecyclerView

/**
 * A [androidx.recyclerview.widget.RecyclerView.ViewHolder] to which can be binded different elements
 * multiple times
 *
 * @param view the holded view
 * @param E the type of the bind items
 */

abstract class BindableViewHolder<E : Any>(view: View) : RecyclerView.ViewHolder(view) {

    /**
     * The currently bind element
     */
    var item: E? = null
        private set

    /**
     * Updates the content of the hold view with the information stored in the given item
     *
     * @param item the new bind item
     */
    @MainThread
    fun bindItem(item: E) {
        this.item = item
        onBindItem(item)
    }

    /**
     * Callback called each time [bindItem] is invoked. Update here the views with the information from the item.
     *
     * @param item the new bind item
     */
    @MainThread
    protected open fun onBindItem(item: E) {
    }
}
