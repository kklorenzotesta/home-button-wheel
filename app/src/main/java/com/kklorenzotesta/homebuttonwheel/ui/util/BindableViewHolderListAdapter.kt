package com.kklorenzotesta.homebuttonwheel.ui.util

import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

/** Base implementation of [ListAdapter] for [BindableViewHolder]s
 *
 * @param builder the [ViewHolderBuilder] from which the [BindableViewHolder]s will be created
 * @param diffCallback the [DiffUtil.Callback] that will be used for the elements, defaults to a [EqualsBasedItemCallback]
 * @param T the type of the elements that will be received
 * @param VH the type of the [BindableViewHolder]
 */
open class BindableViewHolderListAdapter<T : Any, VH : BindableViewHolder<T>>(
    private val builder: ViewHolderBuilder<VH>,
    diffCallback: DiffUtil.ItemCallback<T> = EqualsBasedItemCallback()
) : ListAdapter<T, VH>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        builder(parent)

    @CallSuper
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bindItem(getItem(position))
    }
}
