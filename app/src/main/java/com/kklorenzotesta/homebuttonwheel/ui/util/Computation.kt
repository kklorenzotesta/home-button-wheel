package com.kklorenzotesta.homebuttonwheel.ui.util

/**
 * Represents the status an ongoing computation
 *
 * @param T the output of the computation
 */
sealed class Computation<out T> {
    /** Status of a computation currently loading */
    object Loading : Computation<Nothing>()

    /** Status of an ended computation with his result */
    data class Ready<out T>(val value: T) : Computation<T>()
}
