package com.kklorenzotesta.homebuttonwheel.ui.util

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

/**
 * An implementation of [androidx.recyclerview.widget.DiffUtil.ItemCallback] based on equals.
 */
open class EqualsBasedItemCallback<T> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem
}
