package com.kklorenzotesta.homebuttonwheel.ui.util

import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import java.lang.IllegalStateException

/**
 * A [BindableViewHolder] which provides a new [Lifecycle] each time the current item is changed.
 * Each Lifecycle is initialized on the CREATED state and moved to DESTROYED when the item is changed.
 * If [getLifecycle] is called before any item is bind an IllegalStateException is thrown.
 *
 * **The Lifecycle is not moved to DESTROYED when the ViewHolder is no more used.**
 *
 * @param view the holded view
 * @param E the type of the bind items
 */
abstract class LifecycledViewHolder<E : Any>(
    view: View
) : BindableViewHolder<E>(view), LifecycleOwner {
    /** The [LifecycleRegistry] for the currently displayed item */
    private var lifecycleRegistry: LifecycleRegistry? = null

    /** Returns the [LifecycleRegistry] for the current item, creating it if it doesn't exists */
    private fun getLifecycleRegistry(): LifecycleRegistry =
        lifecycleRegistry ?: if (item != null) LifecycleRegistry(this).also {
            it.currentState = Lifecycle.State.CREATED
            lifecycleRegistry = it
        } else throw IllegalStateException("Lifecycle doesn't exists until an item is binded")

    override fun getLifecycle(): Lifecycle = getLifecycleRegistry()

    @CallSuper
    override fun onBindItem(item: E) {
        getLifecycleRegistry().currentState = Lifecycle.State.DESTROYED
        lifecycleRegistry = null
        super.onBindItem(item)
    }
}
