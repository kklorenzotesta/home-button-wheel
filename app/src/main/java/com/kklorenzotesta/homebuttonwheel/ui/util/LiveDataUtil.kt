package com.kklorenzotesta.homebuttonwheel.ui.util

import androidx.annotation.MainThread
import androidx.lifecycle.*
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.withContext

/**
 * Like observe but after the first value is dispatched the observer will be removed.
 *
 * @see observe
 */
@MainThread
inline fun <T> LiveData<T>.observeOnce(
    owner: LifecycleOwner,
    crossinline onChangedF: (T) -> Unit
): Observer<T> {
    val wrappedObserver = object : Observer<T> {
        override fun onChanged(t: T) {
            removeObserver(this)
            onChangedF(t)
        }
    }
    observe(owner, wrappedObserver)
    return wrappedObserver
}

/** same as [viewModelScope].coroutineContext */
val ViewModel.viewModelContext
    get() = viewModelScope.coroutineContext

/**
 * Converts this Flow in a LiveData.
 * When the flow ends all observer will be removed and all new
 * observers will be notified with the last value and then discarded.
 * see [liveData].
 */
@ExperimentalCoroutinesApi
fun <T> Flow<T>.toLiveData(
    context: CoroutineContext = EmptyCoroutineContext,
    timeoutInMs: Long = 5000L
): LiveData<T> = FlowLiveData(this, context, timeoutInMs)

@ExperimentalCoroutinesApi
private class FlowLiveData<T>(
    flow: Flow<T>,
    context: CoroutineContext = EmptyCoroutineContext,
    timeoutInMs: Long = 5000L
) : MediatorLiveData<T>() {
    private var complete = false
    private val observers = mutableSetOf<WeakReference<Observer<in T>>>()

    init {
        addSource(liveData(
            context,
            timeoutInMs
        ) {
            flow.onCompletion { cause ->
                if (cause == null) {
                    withContext(Dispatchers.Main) {
                        complete = true
                        observers.forEach { observer ->
                            observer.get()?.let(::removeObserver)
                        }
                        observers.clear()
                    }
                }
            }
                .collect {
                    emit(it)
                }
        }) { postValue(it) }
    }

    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        assertMainThread("observe")
        if (!complete) {
            super.observe(owner, observer)
            observers.add(WeakReference(observer))
        } else {
            observer.onChanged(value)
        }
    }

    override fun observeForever(observer: Observer<in T>) {
        assertMainThread("observeForever")
        if (!complete) {
            super.observeForever(observer)
            observers.add(WeakReference(observer))
        } else {
            observer.onChanged(value)
        }
    }
}
