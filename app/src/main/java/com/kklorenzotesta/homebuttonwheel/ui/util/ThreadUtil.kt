package com.kklorenzotesta.homebuttonwheel.ui.util

import androidx.arch.core.executor.ArchTaskExecutor

/** Code taken from LiveData source code */
internal fun assertMainThread(methodName: String) {
    check(ArchTaskExecutor.getInstance().isMainThread) {
        "Cannot invoke $methodName on a background thread"
    }
}
