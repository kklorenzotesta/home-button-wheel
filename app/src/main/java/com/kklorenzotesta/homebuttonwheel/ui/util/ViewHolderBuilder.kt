package com.kklorenzotesta.homebuttonwheel.ui.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.recyclerview.widget.RecyclerView

/** Factory of [androidx.recyclerview.widget.RecyclerView.ViewHolder]s
 *
 * @param layoutRes the id of the layout that will be inflated
 * @param constructor the actual builder of the VH instance after the view is inflated
 * @param VH the type of the ViewHolders that will be created
 */
open class ViewHolderBuilder<out VH : RecyclerView.ViewHolder>(
    @LayoutRes private val layoutRes: Int,
    private val constructor: (View) -> VH
) {
    /**
     * Creates a new VH instance, taking the layout parameters and the context from the given ViewGroup
     *
     * @return a new [VH] instance
     */
    @MainThread
    open operator fun invoke(parent: ViewGroup): VH =
        constructor(parent.inflate(layoutRes))

    private fun ViewGroup.inflate(@LayoutRes layout: Int): View =
        LayoutInflater.from(context).inflate(layout, this, false)
}
