package com.kklorenzotesta.homebuttonwheel.ui.wheel

import androidx.appcompat.app.AppCompatActivity

/**
 * The main activity showing the quick actions on a wheel
 */
class WheelActivity : AppCompatActivity()
