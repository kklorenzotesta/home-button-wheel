package com.kklorenzotesta.homebuttonwheel.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import java.lang.ref.SoftReference
import kotlinx.coroutines.*

/**
 * Describes an async computation that can be started multiple times and always run in [runningScope].
 */
interface AsyncJob {
    /** The scope in which the created [Job] will run */
    val runningScope: CoroutineScope

    /**
     * Starts the computation and expose its computation as a [Job]. The computation
     * is guaranteed to run inside [runningScope]. If the computation has already run there
     * is no guarantee that a new computation will start again.
     */
    fun startAsync(): Job
}

/**
 * Start the given async and add it to this scope,
 * so that if this scope is cancelled the Job is cancelled.
 */
fun CoroutineScope.coStartAsync(async: AsyncJob): Job =
    async.startAsync().addTo(this)

/**
 * Describes an async computation that can be started multiple times and always run in [runningScope].
 *
 * @param T the type async computed value
 */
interface Async<out T : Any> : AsyncJob {
    /**
     * Starts the computation and expose its value as a [Deferred]. The computation
     * is guaranteed to run inside [runningScope]. If the computation has already run there
     * is no guarantee that a new computation will start again, but the value can be cached.
     */
    override fun startAsync(): Deferred<T>
}

/**
 * Start the given async and add it to this scope,
 * so that if this scope is cancelled the Deferred is cancelled.
 */
fun <T : Any> CoroutineScope.coStartAsync(async: Async<T>): Deferred<T> =
    async.startAsync().addTo(this)

/**
 * Basic implementation of [Async] based on a function building [Deferred], each startAsync will
 * build a new Deferred.
 *
 * @param runningScope the scope that will be passed to the builder and **must** be used to
 *                     construct the Deferred
 * @param builder the factory of [Deferred]
 */
class DeferredFactory<out T : Any>(
    override val runningScope: CoroutineScope,
    private val builder: CoroutineScope.() -> Deferred<T>
) : Async<T> {
    override fun startAsync(): Deferred<T> = runningScope.builder()
}

/**
 * Decorator of [Async] caching the result in a [SoftReference].
 *
 * @param wrapped the decorated [Async]
 */
class CachedAsync<out T : Any>(
    private val wrapped: Async<T>
) : Async<T> {
    /** The currently cached result, see [SoftReference] */
    private var cache: SoftReference<Deferred<T>> = SoftReference<Deferred<T>>(null)

    override val runningScope: CoroutineScope
        get() = wrapped.runningScope

    override fun startAsync(): Deferred<T> = cache.get() ?: wrapped.startAsync().also {
        cache = SoftReference(it)
    }
}

/** Returns a new [Async] that caches this async results */
fun <T : Any> Async<T>.cached(): Async<T> = CachedAsync(this)

/** Creates an [Async] running in the scope of this [ViewModel] */
fun <T : Any> ViewModel.async(block: suspend CoroutineScope.() -> T): Async<T> =
    viewModelScope.buildAsync(block)

/** Creates an [Async] running in the scope of the [androidx.lifecycle.Lifecycle] of this [LifecycleOwner] */
fun <T : Any> LifecycleOwner.async(block: suspend CoroutineScope.() -> T): Async<T> =
    lifecycleScope.buildAsync(block)

/** Creates an [Async] with this scope */
fun <T : Any> CoroutineScope.buildAsync(
    block: suspend CoroutineScope.() -> T
): Async<T> = DeferredFactory(this) {
    async { block() }
}
