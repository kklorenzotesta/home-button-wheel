package com.kklorenzotesta.homebuttonwheel.util

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.*

/** Adds this Job to the given scope, so that if the scope is cancelled this Job is cancelled */
fun <T : Job> T.addTo(parent: CoroutineScope): T = addTo(parent.coroutineContext)

/** Adds this Job to the given context, so that if the context is cancelled this Job is cancelled */
fun <T : Job> T.addTo(parent: CoroutineContext): T = this.also { job ->
    parent[Job]?.invokeOnCompletion { job.cancel() }
}

/** Adds this Job to all the given scopes, so that if any scope is cancelled this Job is cancelled */
fun <T : Job> T.addTo(parents: List<CoroutineScope>): T = this.also { job ->
    parents.forEach { job.addTo(it) }
}
