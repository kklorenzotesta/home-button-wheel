package com.kklorenzotesta.homebuttonwheel

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry

fun testContext(): Context =
    InstrumentationRegistry.getInstrumentation().context

fun targetContext(): Context =
    InstrumentationRegistry.getInstrumentation().targetContext
