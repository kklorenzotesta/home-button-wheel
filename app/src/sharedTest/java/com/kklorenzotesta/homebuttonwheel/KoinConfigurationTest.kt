package com.kklorenzotesta.homebuttonwheel

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.koinApplication
import org.koin.test.check.checkModules

@RunWith(AndroidJUnit4::class)
class KoinConfigurationTest {
    @Test
    @SmallTest
    fun checkKoinConfigurationWithTestContext() {
        checkKoinConfiguration(testContext())
    }

    @Test
    @SmallTest
    fun checkKoinConfigurationWithTargetContext() {
        checkKoinConfiguration(targetContext())
    }

    private fun checkKoinConfiguration(context: Context) {
        koinApplication {
            androidContext(context)
            modules(HomeButtonWheel.modules())
        }.apply {
            checkModules()
            close()
        }
    }
}
