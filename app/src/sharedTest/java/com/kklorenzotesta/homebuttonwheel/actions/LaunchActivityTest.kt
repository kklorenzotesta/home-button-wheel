package com.kklorenzotesta.homebuttonwheel.actions

import android.content.Intent
import androidx.test.espresso.intent.Intents.*
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kklorenzotesta.homebuttonwheel.targetContext
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import com.kklorenzotesta.homebuttonwheel.ui.wheel.WheelActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LaunchActivityTest {
    companion object {
        val stub: LaunchActivity by lazy {
            LaunchActivity(
                "packageName",
                "className"
            )
        }
    }

    @get:Rule
    val intentsTestRule = IntentsTestRule(SettingsActivity::class.java)

    @Test
    @SmallTest
    fun runCorrectlyLaunchTheActivity() {
        LaunchActivity(
            targetContext().packageName,
            WheelActivity::class.java.name
        ).run(targetContext())
        intended(toPackage(targetContext().packageName))
        intended(hasFlag(Intent.FLAG_ACTIVITY_NEW_TASK))
        intended(hasComponent(WheelActivity::class.java.name))
    }
}
