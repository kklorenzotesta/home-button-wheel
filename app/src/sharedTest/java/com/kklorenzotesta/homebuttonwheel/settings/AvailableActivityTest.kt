package com.kklorenzotesta.homebuttonwheel.settings

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kklorenzotesta.homebuttonwheel.HomeButtonWheel
import com.kklorenzotesta.homebuttonwheel.testContext
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AvailableActivityTest {
    companion object {
        val stub: AvailableActivity by lazy {
            AvailableActivity(
                "package",
                "fullName"
            )
        }
    }

    @Test
    @SmallTest
    fun componentNameSupportsRoundTrips() {
        val activity =
            AvailableActivity(
                HomeButtonWheel::class.java.`package`!!.name,
                SettingsActivity::class.java.name
            )
        val info = testContext().packageManager.getActivityInfo(activity.componentName, 0)
        assertEquals(activity.packageName, info.packageName)
        assertEquals(activity.fullName, info.name)
    }
}
