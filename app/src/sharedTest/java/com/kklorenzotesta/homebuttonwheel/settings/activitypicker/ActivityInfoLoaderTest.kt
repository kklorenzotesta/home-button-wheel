package com.kklorenzotesta.homebuttonwheel.settings.activitypicker

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.HomeButtonWheel
import com.kklorenzotesta.homebuttonwheel.testContext
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import com.kklorenzotesta.homebuttonwheel.ui.wheel.WheelActivity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ActivityInfoLoaderTest {
    private val loader: ActivityInfoLoader by lazy {
        ActivityInfoLoader(testContext())
    }

    @Test
    @MediumTest
    fun getAvailableActivitiesContainsAllMainActivitiesOfThisApp() = runBlocking {
        assertTrue(loader.getAvailableActivities().first().any {
            it.packageName == HomeButtonWheel::class.java.`package`!!.name &&
                it.fullName == SettingsActivity::class.java.name
        })
        assertTrue(loader.getAvailableActivities().first().any {
            it.packageName == HomeButtonWheel::class.java.`package`!!.name &&
                it.fullName == WheelActivity::class.java.name
        })
    }
}
