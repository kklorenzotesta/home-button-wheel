package com.kklorenzotesta.homebuttonwheel.storage

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kklorenzotesta.homebuttonwheel.testutil.SharedPreferencesTest
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class SharedPreferencesStringStorageTest : SharedPreferencesTest {
    companion object {
        private const val prefKey = "STORAGE_TEST_KEY"
        private const val receiveTimeout = 200L
    }

    private fun getStorage(): StringStorage =
        preferences.toStringStorage()

    @Test
    @SmallTest
    fun setSavesTheGivenValue() = runBlocking {
        val value = "foo"
        getStorage().set(prefKey, value)
        assertEquals(value, preferences.getString(prefKey, null))
    }

    @Test
    @SmallTest
    fun getReturnsNullOnPrefNotSet() = runBlocking {
        val readValue = Channel<String?>()
        val job = GlobalScope.launch {
            getStorage()[prefKey].collect {
                readValue.send(it)
            }
        }
        withTimeout(receiveTimeout) {
            val read = readValue.receive()
            readValue.close()
            job.cancel()
            assertEquals(listOf(null), listOf(read))
        }
    }

    @Test
    @SmallTest
    fun getReturnsCurrentValueAsFirstResult() = runBlocking {
        val value = "bar"
        preferences.edit().apply {
            putString(prefKey, value)
            commit()
        }
        val readValue = Channel<String?>()
        val job = GlobalScope.launch {
            getStorage()[prefKey].collect {
                readValue.send(it)
            }
        }
        withTimeout(receiveTimeout) {
            val read = readValue.receive()
            readValue.close()
            job.cancel()
            assertEquals(listOf(value), listOf(read))
        }
    }

    @Test
    @SmallTest
    fun getReturnsAllUpdates() = runBlocking {
        val values = listOf("foo", "bar", "baz")
        val readValue = Channel<String?>()
        val job = GlobalScope.launch {
            getStorage()[prefKey].collect {
                readValue.send(it)
            }
        }
        withTimeout(receiveTimeout) {
            val read = listOf(readValue.receive()) +
                    values.map {
                        preferences.edit().apply {
                            putString(prefKey, it)
                            commit()
                        }
                        readValue.receive()
                    }
            readValue.close()
            job.cancel()
            assertEquals(listOf(null) + values, read)
        }
    }

    @Test
    @SmallTest
    fun getReturnsUpdatesOnlyForTheGivenKey() = runBlocking {
        val values = listOf("baz")
        val readValue = Channel<String?>()
        val job = GlobalScope.launch {
            getStorage()[prefKey].collect {
                readValue.send(it)
            }
        }
        withTimeout(receiveTimeout) {
            val read = listOf(readValue.receive()) +
                    values.map {
                        preferences.edit().apply {
                            putString(prefKey, it)
                            commit()
                        }
                        preferences.edit().apply {
                            putString(prefKey + "_OTHER", "foo_$it")
                            commit()
                        }
                        readValue.receive()
                    }
            readValue.close()
            job.cancel()
            assertEquals(listOf(null) + values, read)
        }
    }
}
