package com.kklorenzotesta.homebuttonwheel.storage

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.io.FileSender
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import io.mockk.*
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.get

@RunWith(AndroidJUnit4::class)
class WheelActionExporterTest : HBWTest() {
    private val actions = listOf(
        LaunchActivity("a", "b"),
        LaunchActivity("c", "d")
    )

    @Test
    fun exportReturnsNothingToExportOnEmptyList() {
        val exporter = WheelActionExporter(
            mockk(),
            mockk(),
            mockk()
        )
        assertEquals(
            ExportResult.NothingToExport,
            exporter.export(emptyList())
        )
    }

    @Test
    fun exportReturnsSuccessWhenFileSenderSendSucceed() {
        val sender = mockk<FileSender>()
        every { sender.send(any<String>(), any(), any()) } just Runs
        assertEquals(
            ExportResult.Success,
            WheelActionExporter(
                get(),
                sender,
                context
            ).export(actions)
        )
        verify(exactly = 1) { sender.send(any<String>(), any(), any()) }
        confirmVerified(sender)
    }

    @Test
    fun exportGeneratesAFileNameContainingTheAppNameAndTheCorrectExtension() {
        val sender = mockk<FileSender>()
        val slot = slot<String>()
        every { sender.send(any<String>(), capture(slot), any()) } just Runs
        WheelActionExporter(
            get(),
            sender,
            context
        ).export(actions)
        assertTrue(slot.captured.contains(context.getString(R.string.app_name)))
        assertTrue(slot.captured.endsWith("." + context.getString(R.string.backup_files_extension)))
    }

    @Test
    fun exportSetTheTitleFromTheResource() {
        val sender = mockk<FileSender>()
        val slot = slot<String>()
        every { sender.send(any<String>(), any(), capture(slot)) } just Runs
        WheelActionExporter(
            get(),
            sender,
            context
        ).export(actions)
        assertEquals(context.getString(R.string.send_backup_chooser_title), slot.captured)
    }

    @Test
    fun exportReturnsFailure() {
        val sender = mockk<FileSender>()
        val slot = slot<String>()
        every { sender.send(any<String>(), any(), capture(slot)) } just Runs
        WheelActionExporter(
            get(),
            sender,
            context
        ).export(actions)
        assertEquals(context.getString(R.string.send_backup_chooser_title), slot.captured)
    }
}
