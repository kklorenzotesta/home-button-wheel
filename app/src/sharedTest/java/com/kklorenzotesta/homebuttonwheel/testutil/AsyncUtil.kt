package com.kklorenzotesta.homebuttonwheel.testutil

import com.kklorenzotesta.homebuttonwheel.util.Async
import com.kklorenzotesta.homebuttonwheel.util.AsyncJob
import com.kklorenzotesta.homebuttonwheel.util.buildAsync
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main

fun <T : Any> T.toAsync(): Async<T> = { this }.toAsync()

fun <T : Any> (() -> T).toAsync(): Async<T> = CoroutineScope(Main).buildAsync {
    this@toAsync()
}

val noopAsync: AsyncJob = Unit.toAsync()

inline fun <reified T : Any> asyncMockk(): Async<T> = mockk<T>().toAsync()
