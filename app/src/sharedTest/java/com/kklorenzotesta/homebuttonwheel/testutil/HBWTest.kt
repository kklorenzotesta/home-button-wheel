package com.kklorenzotesta.homebuttonwheel.testutil

import android.content.Context
import com.kklorenzotesta.homebuttonwheel.HomeButtonWheel
import com.kklorenzotesta.homebuttonwheel.targetContext
import org.junit.After
import org.junit.Before
import org.koin.core.context.GlobalContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.core.module.Module
import org.koin.test.KoinTest

abstract class HBWTest : KoinTest {
    val context: Context
        get() = targetContext()

    open val module: Module? = null

    open val modules: List<Module> by lazy {
        module?.let(::listOf) ?: emptyList()
    }

    @Before
    fun loadModules() {
        loadKoinModules(modules)
    }

    @After
    fun unloadModules() {
        unloadKoinModules(modules)
        /* Probably the following reload is not needed when run with orchestrator but keep
         * in order to work also without it. This is needed because when an override is
         * unload the original bean is not restored. */
        GlobalContext.get().unloadModules(HomeButtonWheel.modules())
        loadKoinModules(HomeButtonWheel.modules())
    }
}
