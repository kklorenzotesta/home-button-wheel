package com.kklorenzotesta.homebuttonwheel.testutil

import android.content.Context
import android.graphics.drawable.Drawable

fun Context.transparent(): Drawable = getDrawable(android.R.color.transparent)!!
