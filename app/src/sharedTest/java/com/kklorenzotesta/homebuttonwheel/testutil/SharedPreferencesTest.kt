package com.kklorenzotesta.homebuttonwheel.testutil

import android.content.SharedPreferences
import com.kklorenzotesta.homebuttonwheel.testContext
import org.junit.After

interface SharedPreferencesTest {
    companion object {
        private const val sharedPreferencesName: String =
            "ANDROID_INSTRUMENTED_TEST_PREFS"
    }

    val preferences: SharedPreferences
        get() = testContext().getSharedPreferences(sharedPreferencesName, 0)

    @After
    fun clearPreferences() {
        preferences.edit().apply {
            clear()
            commit()
        }
    }
}
