package com.kklorenzotesta.homebuttonwheel.testutil

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.kklorenzotesta.homebuttonwheel.R
import io.mockk.*

inline fun <reified T : Fragment> testNavigation(@IdRes action: Int, test: () -> Unit = {}) {
    val mockNavController = mockk<NavController>(relaxUnitFun = true)
    every { mockNavController.navigate(any<Int>()) } answers {
        mockNavController.navigate(arg<Int>(0), null)
    }
    val fragmentScenario = launchFragmentInContainer<T>(
        themeResId = R.style.AppTheme
    )
    fragmentScenario.onFragment { f ->
        Navigation.setViewNavController(f.requireView(), mockNavController)
    }
    test()
    verify { mockNavController.navigate(action, null) }
}
