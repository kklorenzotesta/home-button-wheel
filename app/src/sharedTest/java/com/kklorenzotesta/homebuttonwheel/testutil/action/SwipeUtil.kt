package com.kklorenzotesta.homebuttonwheel.testutil.action

import android.view.MotionEvent
import android.view.View
import androidx.annotation.IdRes
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.CoordinatesProvider
import androidx.test.espresso.action.GeneralLocation
import androidx.test.espresso.action.GeneralSwipeAction
import androidx.test.espresso.action.MotionEvents
import androidx.test.espresso.action.PrecisionDescriber
import androidx.test.espresso.action.Press
import androidx.test.espresso.action.Swipe
import androidx.test.espresso.action.Swiper
import androidx.test.espresso.action.ViewActions.actionWithAssertions
import java.util.ArrayList

/**
 * Generic implementation of [Swiper].
 *
 * @param holdBefore how long hold down before start swiping
 * @param holdAfter how long hold down before releasing after swiping
 * @param swipeSteps in how many step the swipe must be performed
 * @param swipeDuration how long should take the swipe action only
 */
class HoldSwipe(
    private val holdBefore: Int = 0,
    private val holdAfter: Int = 0,
    private val swipeSteps: Int = 10,
    private val swipeDuration: Int = 150
) : Swiper {
    companion object {
        private fun interpolate(start: FloatArray, end: FloatArray, steps: Int): Array<FloatArray> {
            val res = Array(steps) { FloatArray(2) }
            for (i in 1 until steps + 1) {
                res[i - 1][0] = start[0] + (end[0] - start[0]) * i / (steps + 2f)
                res[i - 1][1] = start[1] + (end[1] - start[1]) * i / (steps + 2f)
            }
            return res
        }
    }

    override fun sendSwipe(
        uiController: UiController,
        startCoordinates: FloatArray,
        endCoordinates: FloatArray,
        precision: FloatArray
    ): Swiper.Status {
        val steps = interpolate(startCoordinates, endCoordinates, swipeSteps)
        val events = ArrayList<MotionEvent>()
        val downEvent = MotionEvents.obtainDownEvent(startCoordinates, precision)
        events.add(downEvent)
        return try {
            val intervalMS = (swipeDuration / steps.size).toLong()
            var eventTime = downEvent.downTime + holdBefore
            for (step in steps) {
                eventTime += intervalMS
                events.add(MotionEvents.obtainMovement(downEvent.downTime, eventTime, step))
            }
            eventTime += intervalMS + holdAfter
            events.add(
                MotionEvent.obtain(
                    downEvent.downTime,
                    eventTime,
                    MotionEvent.ACTION_UP,
                    endCoordinates[0],
                    endCoordinates[1],
                    0
                )
            )
            uiController.injectMotionEventSequence(events)
            Swiper.Status.SUCCESS
        } catch (e: Exception) {
            Swiper.Status.FAILURE
        } finally {
            events.forEach { it.recycle() }
        }
    }
}

/** Creates a swipe [ViewAction] */
fun swipeAction(
    swiper: Swiper = Swipe.FAST,
    from: CoordinatesProvider = GeneralLocation.CENTER,
    to: CoordinatesProvider = GeneralLocation.CENTER,
    with: PrecisionDescriber = Press.FINGER
): ViewAction = actionWithAssertions(
    GeneralSwipeAction(swiper, from, to, with)
)

/** Base this [CoordinatesProvider] on another view found by the passed views */
fun CoordinatesProvider.relativeTo(viewFinder: (View) -> View): CoordinatesProvider =
    CoordinatesProvider {
        this@relativeTo.calculateCoordinates(viewFinder(it))
    }

/** Returns a function finding the first parent of the given type */
inline fun <reified T : View> parentOfType(): (View) -> View = { view ->
    generateSequence(view) { it.parent as View }.find { it is T }!!
}

/** Returns a function finding the child with the given id */
fun childWithId(@IdRes id: Int): (View) -> View = {
    it.findViewById(id)
}
