package com.kklorenzotesta.homebuttonwheel.testutil.matchers

import android.content.Intent
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

class ChooserIntentInnerMatcher(private val innerIntentMatcher: Matcher<Intent>) :
    TypeSafeMatcher<Intent>() {
    override fun describeTo(description: Description) {
        description.appendText("has inner intent with ")
        innerIntentMatcher.describeTo(description)
    }

    override fun matchesSafely(item: Intent): Boolean =
        item.extras?.get(Intent.EXTRA_INTENT).let { inner ->
            inner != null && innerIntentMatcher.matches(inner)
        }
}
