package com.kklorenzotesta.homebuttonwheel.testutil.matchers

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher

class LambdaMatcher<T>(
    private val description: String,
    clazz: Class<T>,
    private val check: (T) -> Boolean
) :
    TypeSafeMatcher<T>(clazz) {
    override fun describeTo(description: Description) {
        description.appendText(this.description)
    }

    override fun matchesSafely(item: T): Boolean =
        check(item)
}

inline fun <reified T : Any> matches(
    description: String,
    noinline check: (T) -> Boolean
): TypeSafeMatcher<T> =
    LambdaMatcher(description, T::class.java, check)

inline fun <reified A : Any, reified T : A> typedMatches(
    description: String,
    noinline check: (T) -> Boolean
): Matcher<A> =
    allOf(
        matches(
            "has type ${T::class.java.name}"
        ) { it is T },
        matches(description) {
            check(it as T)
        }
    )
