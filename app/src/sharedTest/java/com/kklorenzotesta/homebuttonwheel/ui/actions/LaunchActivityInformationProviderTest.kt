package com.kklorenzotesta.homebuttonwheel.ui.actions

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.targetContext
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LaunchActivityInformationProviderTest {
    private val provider = LaunchActivityInformationProvider(targetContext())

    @Test
    @SmallTest
    fun correctLabelIsResolvedForSettingsActivity() =
        runBlocking {
            assertEquals(
                targetContext().getString(R.string.settings_activity_label),
                provider.getLabel(
                    LaunchActivity(
                        targetContext().packageName,
                        SettingsActivity::class.java.name
                    )
                )
            )
        }
}
