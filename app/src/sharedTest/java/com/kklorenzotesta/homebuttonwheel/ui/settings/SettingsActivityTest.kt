package com.kklorenzotesta.homebuttonwheel.ui.settings

import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.widget.Toolbar
import androidx.core.view.children
import androidx.navigation.findNavController
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.targetContext
import com.kklorenzotesta.homebuttonwheel.testutil.matchers.typedMatches
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SettingsActivityTest {
    private fun hasOptionsMenuItems(@IdRes vararg ids: Int): ViewAssertion =
        matches(
            typedMatches<View, Toolbar>(
                "has option menu items with ids ${ids
                    .map { targetContext().resources.getResourceName(it) }}"
            ) { toolbar ->
                toolbar.menu!!.children.map { it.itemId }.toList().containsAll(ids.toList())
            }
        )

    @Test
    @MediumTest
    fun settingsActivityStarts() {
        launchActivity<SettingsActivity>()
    }

    @Test
    @MediumTest
    fun settingsActivityToolbarHasCorrectTitleOnStart() {
        launchActivity<SettingsActivity>()
        onView(withText(targetContext().getString(R.string.settings_activity_label)))
            .check(matches(isDisplayed()))
    }

    @Test
    @MediumTest
    fun settingsActivityToolbarHasOptionsMenu() {
        launchActivity<SettingsActivity>()
        onView(withId(R.id.toolbar))
            .check(hasOptionsMenuItems(R.id.import_settings_menu_item))
            .check(hasOptionsMenuItems(R.id.export_settings_menu_item))
    }

    @Test
    @MediumTest
    fun settingsActivityToolbarHasCorrectTitleAfterNavigationOnAddActivity() {
        launchActivity<SettingsActivity>().onActivity {
            it.findViewById<View>(R.id.settings_nav_host_fragment).findNavController()
                .navigate(R.id.activityPickerFragment)
        }
        onView(withText(targetContext().getString(R.string.activity_picker_fragment_label)))
            .check(matches(isDisplayed()))
    }
}
