package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivityTest
import com.kklorenzotesta.homebuttonwheel.testutil.*
import com.kklorenzotesta.homebuttonwheel.ui.util.Computation
import io.mockk.every
import io.mockk.mockk
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
class MainActivityPickerFragmentTest : HBWTest() {
    override val module: Module? by lazy {
        module {
            viewModel(override = true) {
                mockk<MainActivityPickerViewModel>(relaxUnitFun = true).also {
                    every { it.mainActivities } returns MutableLiveData<
                            Computation<List<DisplayableAvailableActivity>>
                            >().also { mld ->
                        mld.postValue(
                            Computation.Ready(
                                listOf(
                                    DisplayableAvailableActivity(
                                        AvailableActivityTest.stub,
                                        "label",
                                        { context.transparent() }.toAsync(),
                                        noopAsync
                                    )
                                )
                            )
                        )
                    }
                }
            }
        }
    }

    @Test
    @MediumTest
    fun onActivityClickNavigatesBack() {
        testNavigation<MainActivityPickerFragment>(R.id.action_backToEditSettings) {
            onView(withId(R.id.main_activity_name)).perform(click())
        }
    }
}
