package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.RootMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.storage.ExportResult
import com.kklorenzotesta.homebuttonwheel.storage.WheelActionExporter
import com.kklorenzotesta.homebuttonwheel.storage.WheelActionStorage
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import com.kklorenzotesta.homebuttonwheel.ui.settings.SettingsActivity
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
open class EditSettingsFragmentExportTest : HBWTest() {
    private val storage = mockk<WheelActionStorage>()
    private val exporter = mockk<WheelActionExporter>()

    override val module: Module? by lazy {
        module {
            viewModel(override = true) {
                EditSettingsViewModel(
                    storage,
                    exporter,
                    mockk(),
                    mockk()
                )
            }
        }
    }

    @Before
    fun resetMock() {
        clearMocks(storage, exporter)
    }

    @Test
    @Ignore(
        "On android hangs forever and on Robolectric throws exception," +
                " need to upgrade Espresso in order to work"
    )
    fun exportWithoutAnySettingShowsAToast() {
        every { storage.getActions() } returns flowOf(emptyList())
        every { exporter.export(any()) } returns ExportResult.NothingToExport
        val scenario = launchActivity<SettingsActivity>()
        openActionBarOverflowOrOptionsMenu(context)
        onView(withText(context.getString(R.string.export_settings_menu_item_title)))
            .perform(click())
        scenario.onActivity { activity ->
            onView(withText(R.string.nothing_to_export_error_message))
                .inRoot(withDecorView(not(activity.window.decorView)))
                .check(matches(isDisplayed()))
        }
    }
}
