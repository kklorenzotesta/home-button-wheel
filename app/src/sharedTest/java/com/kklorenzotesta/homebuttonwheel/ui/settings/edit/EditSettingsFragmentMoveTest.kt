package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.GeneralLocation
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivityTest
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import com.kklorenzotesta.homebuttonwheel.testutil.action.*
import com.kklorenzotesta.homebuttonwheel.testutil.noopAsync
import com.kklorenzotesta.homebuttonwheel.testutil.toAsync
import com.kklorenzotesta.homebuttonwheel.testutil.transparent
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
open class EditSettingsFragmentMoveTest : HBWTest() {
    private val actions: List<DisplayableWheelAction> = listOf(
        DisplayableWheelAction(
            LaunchActivityTest.stub,
            "Label".toAsync(),
            { context.transparent() }.toAsync(),
            noopAsync
        ),
        DisplayableWheelAction(
            LaunchActivityTest.stub.copy(packageName = "packageName2"),
            "Label2".toAsync(),
            { context.transparent() }.toAsync(),
            noopAsync
        )
    )

    override val module: Module? by lazy {
        module {
            viewModel(override = true) {
                mockk<EditSettingsViewModel>(relaxUnitFun = true).also {
                    every { it.actions } returns MutableLiveData<List<DisplayableWheelAction>>()
                        .also { mld ->
                            mld.postValue(actions)
                        }
                    every { it.setActions(any()) } answers {
                        lastSetActions = arg(0)
                    }
                }
            }
        }
    }

    private var lastSetActions: List<DisplayableWheelAction>? = null

    @Before
    fun clearLastSetActions() {
        lastSetActions = null
    }

    @Test
    @MediumTest
    @Ignore("infinite loop on robolectric")
    open fun canReorderWithHandle() {
        checkReorder(
            swipeAction(
                from = GeneralLocation.CENTER.relativeTo(childWithId(R.id.list_item_handle)),
                to = GeneralLocation.TOP_CENTER.relativeTo(parentOfType<RecyclerView>())
            )
        )
    }

    @Test
    @MediumTest
    @Ignore("infinite loop on robolectric")
    open fun canReorderWithLongPress() {
        checkReorder(
            swipeAction(
                swiper = HoldSwipe(holdBefore = 600),
                to = GeneralLocation.TOP_CENTER.relativeTo(parentOfType<RecyclerView>())
            )
        )
    }

    private fun checkReorder(swipeAction: ViewAction) {
        launchFragmentInContainer<EditSettingsFragment>(
            themeResId = R.style.AppTheme
        )
        onView(withId(R.id.actions_list))
            .perform(
                actionOnItemAtPosition<WheelActionViewHolder>(1, swipeAction)
            )
        assertEquals(actions.reversed(), lastSetActions)
    }
}
