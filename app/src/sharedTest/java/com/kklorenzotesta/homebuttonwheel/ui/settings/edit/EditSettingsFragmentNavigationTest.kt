package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.testutil.testNavigation
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditSettingsFragmentNavigationTest {
    @Test
    @MediumTest
    fun fabClickTriggersNavigation() {
        testNavigation<EditSettingsFragment>(R.id.action_addActivityAction) {
            onView(withId(R.id.add_action_button)).check(matches(isDisplayed()))
                .perform(click())
        }
    }
}
