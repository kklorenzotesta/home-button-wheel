package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.kklorenzotesta.homebuttonwheel.R
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivityTest
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.testutil.HBWTest
import com.kklorenzotesta.homebuttonwheel.testutil.toAsync
import com.kklorenzotesta.homebuttonwheel.testutil.transparent
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
class EditSettingsFragmentSwipeTest : HBWTest() {
    override val module: Module? by lazy {
        module {
            viewModel(override = true) {
                mockk<EditSettingsViewModel>(relaxUnitFun = true).also {
                    every { it.actions } returns MutableLiveData<
                            List<DisplayableWheelAction>
                            >().also { mld ->
                        mld.postValue(
                            listOf(
                                DisplayableWheelAction(
                                    LaunchActivityTest.stub,
                                    "Label".toAsync(),
                                    { context.transparent() }.toAsync(),
                                    { lastDeletedAction = LaunchActivityTest.stub }.toAsync()
                                )
                            )
                        )
                    }
                }
            }
        }
    }

    private var lastDeletedAction: WheelAction? = null

    @Before
    fun clearLastDeleted() {
        lastDeletedAction = null
    }

    @Test
    @MediumTest
    @Ignore("test doesn't works")
    fun swipeLeftDeletesTheAction() {
        testSwipe(swipeLeft())
    }

    @Test
    @MediumTest
    fun swipeRightsDeletesTheAction() {
        testSwipe(swipeRight())
    }

    private fun testSwipe(swipe: ViewAction) {
        launchFragmentInContainer<EditSettingsFragment>(
            themeResId = R.style.AppTheme
        )
        onView(withId(R.id.actions_list_action_layout))
            .check(matches(isDisplayed()))
            .perform(swipe)
        assertEquals(LaunchActivityTest.stub, lastDeletedAction)
    }
}
