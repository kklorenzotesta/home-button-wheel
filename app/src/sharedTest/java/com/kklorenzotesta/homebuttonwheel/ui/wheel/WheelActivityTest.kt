package com.kklorenzotesta.homebuttonwheel.ui.wheel

import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WheelActivityTest {
    @Test
    @MediumTest
    fun wheelActivityStarts() {
        launchActivity<WheelActivity>()
    }
}
