package com.kklorenzotesta.homebuttonwheel

import io.mockk.mockk
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.koinApplication
import org.koin.test.check.checkModules

class KoinConfigurationUnitTest {
    @Test
    fun `check koin modules`() {
        koinApplication {
            androidContext(mockk(relaxed = true))
            modules(HomeButtonWheel.modules())
        }.checkModules()
    }
}
