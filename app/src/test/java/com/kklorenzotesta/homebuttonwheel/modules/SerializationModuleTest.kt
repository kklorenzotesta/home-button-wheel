package com.kklorenzotesta.homebuttonwheel.modules

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.testutil.HBWKoinTest
import kotlinx.serialization.KSerializer
import kotlinx.serialization.PolymorphicSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonLiteral
import kotlinx.serialization.json.JsonObject
import org.junit.Assert.assertEquals
import org.junit.Test
import org.koin.test.inject

class SerializationModuleTest : HBWKoinTest() {
    private val json: Json by inject()
    private val serializer: KSerializer<Any> by lazy {
        PolymorphicSerializer(WheelAction::class)
    }
    private val launchActivity = LaunchActivity("package", "name")

    @Test
    fun `deserialization ignores extra fields`() {
        val jsonObject = JsonObject(
            json.toJson(serializer, launchActivity).jsonObject + ("extraField" to JsonLiteral(43))
        )
        assertEquals(
            launchActivity, json.parse(
                serializer, json.stringify(
                    JsonObject.serializer(), jsonObject
                )
            )
        )
    }

    @Test
    fun `LaunchActivity can perform round trips`() {
        assertEquals(
            launchActivity, json.parse(
                serializer, json.stringify(
                    serializer, launchActivity
                )
            )
        )
    }

    @Test
    fun `LaunchActivity serial form is stable`() {
        val packageName = "com.adobe.reader"
        val className = "com.adobe.reader.AdobeReader"
        val serialForm = """
            {"type":"com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity","packageName":"$packageName","className":"$className"}
        """.trimIndent()
        assertEquals(
            LaunchActivity(packageName, className),
            json.parse(serializer, serialForm)
        )
    }
}
