package com.kklorenzotesta.homebuttonwheel.serialization

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.modules.ACTIONS_SERIALIZER_NAME
import com.kklorenzotesta.homebuttonwheel.testutil.HBWKoinTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.koin.core.qualifier.named
import org.koin.test.get

class SerializerTest : HBWKoinTest() {
    private val serializer: Serializer by lazy {
        Serializer(
            get(),
            get(named(ACTIONS_SERIALIZER_NAME))
        )
    }

    @Test
    fun `Serializer can perform round trips of lists of WheelAction`() {
        val actions: List<WheelAction> =
            listOf(LaunchActivity("p", "c"), LaunchActivity("p2", "c2"))
        assertEquals(
            actions,
            serializer.parseActions(
                serializer.stringify(actions)
            )
        )
    }
}
