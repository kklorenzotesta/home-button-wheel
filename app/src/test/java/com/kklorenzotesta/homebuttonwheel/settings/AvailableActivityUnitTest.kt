package com.kklorenzotesta.homebuttonwheel.settings

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import org.junit.Assert.*
import org.junit.Test

class AvailableActivityUnitTest {
    @Test
    fun `toWheelAction creates a correct LaunchActivity`() {
        val a = AvailableActivity("packageName", "fullName")
        val w = a.toWheelAction()
        assertTrue(w is LaunchActivity)
        w as LaunchActivity
        assertEquals(a.packageName, w.packageName)
        assertEquals(a.fullName, w.className)
    }
}
