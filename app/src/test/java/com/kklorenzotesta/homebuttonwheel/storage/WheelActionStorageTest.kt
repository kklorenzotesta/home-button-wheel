package com.kklorenzotesta.homebuttonwheel.storage

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.testutil.HBWKoinTest
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.PolymorphicSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import org.junit.Assert.*
import org.junit.Test
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.test.get

class WheelActionStorageTest : HBWKoinTest() {
    private class ConfigurableStringStorage(
        var values: List<String?> = emptyList()
    ) : StringStorage {
        var setted: List<String?> = emptyList()
        override fun get(key: String): Flow<String?> =
            values.asFlow()

        override suspend fun set(key: String, value: String?) {
            setted = setted + value
        }
    }

    private val storage = ConfigurableStringStorage()

    override val modules: List<Module>
        get() = super.modules + module {
            single<StringStorage>(override = true) { storage }
        }

    private fun List<WheelAction>.write() =
        get<Json>().stringify(PolymorphicSerializer(WheelAction::class).list, this)

    @Test
    fun `getActions returns empty list on missing values`() = runBlocking {
        storage.values = listOf(null)
        var callCounter = 0
        get<WheelActionStorage>().getActions().collect {
            callCounter++
            assertEquals(emptyList<WheelAction>(), it)
        }
        assertEquals(1, callCounter)
    }

    @Test
    fun `getActions correctly parse strings`() = runBlocking {
        var callCounter = 0
        val action = LaunchActivity("", "")
        storage.values = listOf(listOf(action).write())
        get<WheelActionStorage>().getActions().collect {
            callCounter++
            assertEquals(listOf(action), it)
        }
        assertEquals(1, callCounter)
    }

    @Test
    fun `addWheelAction persists with added action on empty list`() =
        runBlocking {
            val action = LaunchActivity("", "")
            storage.setted = emptyList()
            storage.values = listOf(null)
            get<WheelActionStorage>().addWheelAction(action)
            assertEquals(listOf(action).write(), storage.setted.last())
        }

    @Test
    fun `addWheelAction persists with added action in last position`() =
        runBlocking {
            val actions = listOf(
                LaunchActivity("a", ""),
                LaunchActivity("b", "")
            )
            val action = LaunchActivity("c", "")
            storage.setted = emptyList()
            storage.values = listOf(actions.write())
            get<WheelActionStorage>().addWheelAction(action)
            assertEquals((actions + action).write(), storage.setted.last())
        }

    @Test
    fun `removeWheelAction persist null on already empty list`() =
        runBlocking {
            val action = LaunchActivity("", "")
            storage.setted = emptyList()
            storage.values = listOf(null)
            get<WheelActionStorage>().removeWheelAction(action)
            assertNull(storage.setted.last())
        }

    @Test
    fun `removeWheelAction removes first matching element`() =
        runBlocking {
            val actions = listOf(
                LaunchActivity("a", ""),
                LaunchActivity("b", ""),
                LaunchActivity("c", "")
            )
            storage.setted = emptyList()
            storage.values = listOf(actions.write())
            get<WheelActionStorage>().removeWheelAction(actions.first())
            assertEquals(actions.drop(1).write(), storage.setted.last())
        }

    @Test
    fun `setActions persists null on empty list`() =
        runBlocking {
            storage.setted = emptyList()
            get<WheelActionStorage>().setActions(emptyList())
            assertNull(storage.setted.last())
        }

    @Test
    fun `setActions persists exactly the list`() =
        runBlocking {
            val actions = listOf(
                LaunchActivity("a", ""),
                LaunchActivity("b", ""),
                LaunchActivity("c", "")
            )
            storage.setted = emptyList()
            get<WheelActionStorage>().setActions(actions)
            assertEquals(actions.write(), storage.setted.last())
        }
}
