package com.kklorenzotesta.homebuttonwheel.testutil

import com.kklorenzotesta.homebuttonwheel.HomeButtonWheel
import org.junit.After
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.Module
import org.koin.test.KoinTest

abstract class HBWKoinTest : KoinTest {
    open val modules: List<Module> = HomeButtonWheel.modules()

    @Before
    fun startKoinBeforeTests() {
        startKoin {
            modules(modules)
        }
    }

    @After
    fun stopKoinAfterTests() {
        stopKoin()
    }
}
