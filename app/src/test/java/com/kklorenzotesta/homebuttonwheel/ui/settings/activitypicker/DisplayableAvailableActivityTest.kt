package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test

class DisplayableAvailableActivityTest {
    private fun instance() = DisplayableAvailableActivity(
        AvailableActivity("package", "class"),
        "label",
        mockk(),
        mockk()
    )

    @Test
    fun `equals to itself is true`() {
        val i = instance()
        assertEquals(i, i)
        assertEquals(i, instance())
    }

    @Test
    fun `equals only on some field`() {
        val i = instance()
        assertEquals(
            i, DisplayableAvailableActivity(
                i.activity,
                i.label + "0",
                mockk(),
                mockk()
            )
        )
    }
}
