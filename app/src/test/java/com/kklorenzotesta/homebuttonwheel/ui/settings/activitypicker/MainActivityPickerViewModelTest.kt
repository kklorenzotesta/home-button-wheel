package com.kklorenzotesta.homebuttonwheel.ui.settings.activitypicker

import com.jraska.livedata.test
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.actions.WheelAction
import com.kklorenzotesta.homebuttonwheel.settings.AvailableActivity
import com.kklorenzotesta.homebuttonwheel.settings.activitypicker.ActivityInfoLoader
import com.kklorenzotesta.homebuttonwheel.testutil.SingleThreadedUnitTest
import com.kklorenzotesta.homebuttonwheel.ui.actions.WheelActionInformationProvider
import com.kklorenzotesta.homebuttonwheel.ui.util.Computation
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.withTimeout
import org.junit.Test

@ExperimentalCoroutinesApi
class MainActivityPickerViewModelTest : SingleThreadedUnitTest() {
    @Test
    fun `mainActivities are ordered by ascending label`() = runBlockingTest {
        withTimeout(30000) {
            val activities = listOf(
                AvailableActivity("a", "b"),
                AvailableActivity("b", "c"),
                AvailableActivity("c", "a")
            )
            val loader: ActivityInfoLoader = mockk()
            every { loader.getAvailableActivities() } returns flow {
                emit(activities)
            }
            val infoProvider: WheelActionInformationProvider<WheelAction> = mockk()
            coEvery { infoProvider.getLabel(any()) } answers {
                (arg<WheelAction>(0) as LaunchActivity).className
            }
            val vm = MainActivityPickerViewModel(
                loader,
                infoProvider,
                mockk()
            )
            vm.mainActivities.test()
                .awaitValue()
                .assertHasValue()
                .assertValue(Computation.Loading)
                .awaitNextValue()
                .assertValue { it is Computation.Ready }
                .assertValue { c ->
                    (c as Computation.Ready).value.map { it.activity }.toSet() == activities.toSet()
                }
                .assertValue { c ->
                    (c as Computation.Ready).value.let { v ->
                        v == v.sortedBy { it.label }
                    }
                }
        }
    }
}
