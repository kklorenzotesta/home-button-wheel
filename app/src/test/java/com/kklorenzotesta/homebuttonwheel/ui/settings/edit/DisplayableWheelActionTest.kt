package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test

class DisplayableWheelActionTest {
    private fun instance() = DisplayableWheelAction(
        LaunchActivity("package", "class"),
        mockk(),
        mockk(),
        mockk()
    )

    @Test
    fun `equals to itself is true`() {
        val i = instance()
        assertEquals(i, i)
        assertEquals(i, instance())
    }

    @Test
    fun `equals only on some field`() {
        val i = instance()
        assertEquals(
            i, DisplayableWheelAction(
                i.action,
                mockk(),
                mockk(),
                mockk()
            )
        )
    }
}
