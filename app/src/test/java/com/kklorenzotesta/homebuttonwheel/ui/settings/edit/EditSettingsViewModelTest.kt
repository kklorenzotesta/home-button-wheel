package com.kklorenzotesta.homebuttonwheel.ui.settings.edit

import android.net.Uri
import com.jraska.livedata.test
import com.kklorenzotesta.homebuttonwheel.actions.LaunchActivity
import com.kklorenzotesta.homebuttonwheel.storage.*
import com.kklorenzotesta.homebuttonwheel.testutil.SingleThreadedUnitTest
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Test

@ExperimentalCoroutinesApi
class EditSettingsViewModelTest : SingleThreadedUnitTest() {
    @Test
    fun `creates DisplayableWheelActions with correct deleteAction`() =
        runBlocking {
            val storage = mockk<WheelActionStorage>()
            val action = LaunchActivity("foo", "bar")
            coEvery { storage.removeWheelAction(any()) } just Runs
            every { storage.getActions() } returns flowOf(listOf(action))
            val vm = EditSettingsViewModel(storage, mockk(), mockk(), mockk())
            vm.actions.test()
                .awaitValue()
                .assertHasValue()
                .assertValue {
                    1 == it.size
                }
                .value()[0].deleteAction.startAsync().join()
            coVerify(exactly = 1) { storage.removeWheelAction(action) }
        }

    @Test
    fun `exportActions uses the current actions and pass the result`() =
        runBlocking {
            val storage = mockk<WheelActionStorage>()
            val exporter = mockk<WheelActionExporter>()
            val action = LaunchActivity("foo", "bar")
            every { storage.getActions() } returns flowOf(listOf(action), listOf())
            every { exporter.export(any()) } returns ExportResult.Success
            val vm = EditSettingsViewModel(storage, exporter, mockk(), mockk())
            vm.exportActions().test()
                .awaitValue()
                .assertHasValue()
                .assertValue(ExportResult.Success)
            verify(exactly = 1) { exporter.export(listOf(action)) }
        }

    @Test
    fun `importActions pass the given uri and returns the result`() = runBlocking {
        val storage = mockk<WheelActionStorage>()
        every { storage.getActions() } returns flowOf(emptyList())
        val importer = mockk<WheelActionImporter>()
        coEvery { importer.import(any()) } returns ImportResult.Success
        val uri: Uri = mockk()
        val vm = EditSettingsViewModel(storage, mockk(), importer, mockk())
        vm.importActions(uri).test()
            .awaitValue()
            .assertHasValue()
            .assertValue(ImportResult.Success)
        coVerify(exactly = 1) { importer.import(uri) }
    }
}
