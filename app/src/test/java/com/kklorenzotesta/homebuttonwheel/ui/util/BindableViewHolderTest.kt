package com.kklorenzotesta.homebuttonwheel.ui.util

import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test

open class BindableViewHolderTest {
    protected open fun build(onBindItemAction: (String) -> Unit = {}): BindableViewHolder<String> =
        object : BindableViewHolder<String>(mockk()) {
            override fun onBindItem(item: String) {
                onBindItemAction(item)
            }
        }

    @Test
    fun `onBindItem is called after each bindItem`() {
        var counter = 0
        val viewHolder = build { counter++ }
        viewHolder.bindItem("")
        viewHolder.bindItem("")
        assertEquals(2, counter)
    }

    @Test
    fun `item contains last binded item`() {
        val viewHolder = build()
        assertNull(viewHolder.item)
        viewHolder.bindItem("")
        viewHolder.bindItem("t")
        viewHolder.bindItem("")
        assertEquals("", viewHolder.item)
    }
}
