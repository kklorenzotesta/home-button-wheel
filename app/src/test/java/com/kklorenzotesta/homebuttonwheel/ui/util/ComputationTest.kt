package com.kklorenzotesta.homebuttonwheel.ui.util

import org.junit.Test

class ComputationTest {
    @Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE", "RemoveExplicitTypeArguments")
    @Test
    fun `Computation is covariant`() {
        var c: Computation<Any>? = null
        c = Computation.Loading
        c = Computation.Ready<String>("string")
        var r: Computation.Ready<Any>? = null
        r = Computation.Ready<Int>(9)
    }
}
