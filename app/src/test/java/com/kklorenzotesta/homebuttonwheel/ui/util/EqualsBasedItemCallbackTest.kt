package com.kklorenzotesta.homebuttonwheel.ui.util

import org.junit.Assert.*
import org.junit.Test

class EqualsBasedItemCallbackTest {
    private class A(val output: Boolean) {
        var equalsIsCalled: Boolean = false
            private set
        override fun equals(other: Any?): Boolean {
            equalsIsCalled = true
            return output
        }
    }

    private val callback = EqualsBasedItemCallback<A>()

    @Test
    fun equalsIsCalledOnAreItemsTheSame() {
        val a = A(true)
        callback.areItemsTheSame(a, A(true))
        assertTrue(a.equalsIsCalled)
    }

    @Test
    fun equalsIsCalledOnAreContentsTheSame() {
        val a = A(true)
        callback.areContentsTheSame(a, A(true))
        assertTrue(a.equalsIsCalled)
    }

    @Test
    fun equalsValueIsReturned() {
        assertTrue(callback.areItemsTheSame(A(true), A(true)))
        assertFalse(callback.areItemsTheSame(A(false), A(false)))
        assertTrue(callback.areContentsTheSame(A(true), A(true)))
        assertFalse(callback.areContentsTheSame(A(false), A(false)))
    }
}
