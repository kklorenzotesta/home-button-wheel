package com.kklorenzotesta.homebuttonwheel.ui.util

import androidx.lifecycle.Lifecycle
import io.mockk.mockk
import java.lang.IllegalStateException
import org.junit.Assert.*
import org.junit.Test

open class LifecycledViewHolderTest : BindableViewHolderTest() {
    override fun build(onBindItemAction: (String) -> Unit): LifecycledViewHolder<String> =
        object : LifecycledViewHolder<String>(mockk()) {
            override fun onBindItem(item: String) {
                super.onBindItem(item)
                onBindItemAction(item)
            }
        }

    @Test
    fun `getLifecycle throw IllegalStateException before the first bind`() {
        val viewModel = build()
        try {
            viewModel.lifecycle
            fail()
        } catch (e: IllegalStateException) {
        }
    }

    @Test
    fun `Lifecycle is created in state CREATED`() {
        val viewModel = build()
        viewModel.bindItem("")
        assertEquals(Lifecycle.State.CREATED, viewModel.lifecycle.currentState)
    }

    @Test
    fun `Lifecycle is moved to DESTROYED stated after new bind`() {
        val viewModel = build()
        viewModel.bindItem("")
        val lifecycle = viewModel.lifecycle
        viewModel.bindItem("")
        assertEquals(Lifecycle.State.DESTROYED, lifecycle.currentState)
    }

    @Test
    fun `A new Lifecycle is created after each bind`() {
        val viewModel = build()
        viewModel.bindItem("")
        val lifecycle1 = viewModel.lifecycle
        viewModel.bindItem("")
        val lifecycle2 = viewModel.lifecycle
        assertNotEquals(lifecycle1, lifecycle2)
    }
}
