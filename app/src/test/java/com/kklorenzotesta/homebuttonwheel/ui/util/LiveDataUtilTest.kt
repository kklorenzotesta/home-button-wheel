package com.kklorenzotesta.homebuttonwheel.ui.util

import androidx.lifecycle.*
import com.jraska.livedata.test
import com.kklorenzotesta.homebuttonwheel.testutil.SingleThreadedUnitTest
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Ignore
import org.junit.Test

@ExperimentalCoroutinesApi
class LiveDataUtilTest : SingleThreadedUnitTest() {
    class Owner : LifecycleOwner {
        val lifecycle = LifecycleRegistry(this).apply {
            currentState = Lifecycle.State.RESUMED
        }

        override fun getLifecycle(): Lifecycle =
            lifecycle
    }

    @Test
    fun `observeOnce unregisters the observer after receiving the first value`() {
        val liveData = spyk(MutableLiveData<String>())
        val owner = Owner()
        val receivedValues = mutableListOf<String>()
        val observer = liveData.observeOnce(owner) {
            receivedValues.add(it)
        }
        liveData.value = "a"
        liveData.value = "b"
        assertEquals(listOf("a"), receivedValues)
        verify { liveData.removeObserver(observer) }
    }

    @Test
    @Ignore("test result is not deterministic")
    fun `toLiveData returns all elements`() {
        val values = listOf("a", "b", "b", "c", "a")
        var emittedCounter = 0
        val liveData = flowOf(*values.toTypedArray()).map {
            emittedCounter++
            it
        }.toLiveData()
        val t = liveData.test()
        while (emittedCounter < values.size) {
            t.awaitNextValue()
        }
        t.assertValueHistory(*values.toTypedArray())
    }

    @Test
    @Ignore("don't know how to test")
    fun `toLiveData removes the observer on flow end`() {
        fail()
    }

    @Test
    @Ignore("don't know how to test")
    fun `toLiveData doesn't actually adds the observers after is complete`() {
        fail()
    }
}
