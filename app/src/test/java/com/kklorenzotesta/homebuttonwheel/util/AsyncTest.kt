package com.kklorenzotesta.homebuttonwheel.util

import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import kotlinx.coroutines.*
import org.junit.Assert.*
import org.junit.Test

class AsyncTest {
    @Test
    fun `DeferredFactory pass the correct context to the builder`() =
        runBlocking {
            val scope = CoroutineScope(Job())
            val factory = DeferredFactory(scope) {
                val passed = this
                async { assertEquals(scope, passed) }
            }
            factory.startAsync().await()
        }

    @Test
    fun `DeferredFactory created Deferred is cancelled when scope is cancelled`() =
        runBlocking {
            val scope = CoroutineScope(Job())
            val factory = DeferredFactory(scope) {
                async { delay(Long.MAX_VALUE) }
            }
            val deferred = factory.startAsync()
            scope.cancel()
            assertTrue(deferred.isCancelled)
        }

    @Test
    fun `CachedAsync correctly cache the wrapped Async output`() =
        runBlocking {
            var counter = 0
            val async = buildAsync {
                counter++
            }.cached()
            val firstOutput = async.startAsync().await()
            assertEquals(firstOutput, async.startAsync().await())
        }

    @Test
    fun `CachedAsync runningScope is the original runningScope`() =
        runBlocking {
            val async = GlobalScope.buildAsync {
            }
            val cached = async.cached()
            assertEquals(async.runningScope, cached.runningScope)
        }

    @Test
    fun `ViewModel extension method pass the correct scope`() {
        val vm: ViewModel = mockk()
        val vms: CoroutineScope = mockk()
        every { vm.viewModelScope } returns vms
        val async = vm.async { }
        assertEquals(vms, async.runningScope)
    }

    @Test
    fun `LifecycleOwner extension method pass the correct scope`() {
        val lo: LifecycleOwner = mockk()
        val lco: LifecycleCoroutineScope = mockk()
        mockkStatic("androidx.lifecycle.LifecycleKt")
        every { lo.lifecycleScope } returns lco
        val async = lo.async { }
        assertEquals(lco, async.runningScope)
        unmockkStatic("androidx.lifecycle.LifecycleKt")
    }

    @Test
    fun `coStartAsync adds the scope to this context`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val scope2 = CoroutineScope(Job())
            val factory = DeferredFactory(scope1) {
                async { delay(Long.MAX_VALUE) }
            } as AsyncJob
            val deferred = scope2.coStartAsync(factory)
            scope2.cancel()
            assertTrue(deferred.isCancelled)
            scope1.cancel()
        }

    @Test
    fun `coStartAsync adds the deferred to this context`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val scope2 = CoroutineScope(Job())
            val factory = DeferredFactory(scope1) {
                async { delay(Long.MAX_VALUE) }
            }
            val deferred = scope2.coStartAsync(factory)
            scope2.cancel()
            assertTrue(deferred.isCancelled)
            scope1.cancel()
        }
}
