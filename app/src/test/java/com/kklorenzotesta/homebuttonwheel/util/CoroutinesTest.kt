package com.kklorenzotesta.homebuttonwheel.util

import kotlinx.coroutines.*
import org.junit.Assert.*
import org.junit.Test

class CoroutinesTest {
    @Test
    fun `when a scope is add when the original scope is cancelled coroutine is cancelled`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val scope2 = CoroutineScope(Job())
            val job = scope1.launch {
                delay(Long.MAX_VALUE)
            }
            job.addTo(scope2)
            scope1.cancel()
            assertTrue(job.isCancelled)
            scope2.cancel()
        }

    @Test
    fun `when additional scope is cancelled coroutine is cancelled`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val scope2 = CoroutineScope(Job())
            val job = scope1.launch {
                delay(Long.MAX_VALUE)
            }
            job.addTo(scope2)
            scope2.cancel()
            assertTrue(job.isCancelled)
            scope1.cancel()
        }

    @Test
    fun `when additional context is cancelled coroutine is cancelled`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val scope2 = CoroutineScope(Job())
            val job = scope1.launch {
                delay(Long.MAX_VALUE)
            }
            job.addTo(scope2.coroutineContext)
            scope2.cancel()
            assertTrue(job.isCancelled)
            scope1.cancel()
        }

    @Test
    fun `when any additional scope is cancelled coroutine is cancelled`() =
        runBlocking {
            val scope1 = CoroutineScope(Job())
            val otherScopes = MutableList(8) { CoroutineScope(Job()) }
            val job = scope1.launch {
                delay(Long.MAX_VALUE)
            }
            job.addTo(otherScopes)
            otherScopes.random().cancel()
            assertTrue(job.isCancelled)
            scope1.cancel()
            otherScopes.forEach { it.cancel() }
        }
}
